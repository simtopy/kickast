#!/usr/bin/env bash
if [ -e cjs-out ] ; then rm -rf cjs-out ; fi
./node_modules/@babel/cli/bin/babel.js --plugins @babel/plugin-transform-modules-commonjs lib -d cjs-out/lib &&
./node_modules/@babel/cli/bin/babel.js --plugins @babel/plugin-transform-modules-commonjs index.js test.js -d cjs-out &&
cp ./package.json ./cjs-out/package.json &&
cp ./package-lock.json ./cjs-out/package-lock.json &&
cp ./README.md ./cjs-out/README.md &&
cp ./LICENSE ./cjs-out/LICENSE &&
cp ./tsconfig.json ./cjs-out/tsconfig.json &&
sed -i 's/@simtopy\/kickast/@simtopy\/kickast-cjs/g' ./cjs-out/package.json &&
sed -i 's/"type": "module"/"type": "commonjs"/g' ./cjs-out/package.json &&

#Monkey patching
sed -i 's/(await/ \/\/\
require\("astring"\).generate \/\/ /g' ./cjs-out/lib/index.js &&

sed -i 's/_acorn.default.parse;/require\("acorn"\).parse/g' ./cjs-out/lib/internal.js