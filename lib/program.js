import * as Internal from './internal.js';

/**
 *
 *
 * @class Program
 * @extends {Internal.StatementContainer}
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#programs}
 */
class Program extends Internal.StatementContainer {
  constructor(instance) {
    super(null)
    this.comments = []
    this.imports = []
    this.body = []
    this.exports = []
    this.kickastInstance = instance
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      comments: this.comments,
      body: [
        ...this.imports.map(i => i.node),
        ...this.body.map(s => s.node),
        ...this.exports.map(e => e.node),
      ]
    }
  }

  set import(i) {
    this.imports.push(i)
  }

  set export(e) {
    this.exports.push(e)
  }

  set statement(s) {
    this.body.push(s)
  }

  /**
   * Generate the litteral representation of this node
   *
   * @param options? - the generator arguments
   **/
  toString(options) {
    const mergeOptions = {}
    Object.assign(mergeOptions, this.kickastInstance.options.generateOptions)
    Object.assign(mergeOptions, options)
    const res = this.kickastInstance.generate(this.node, mergeOptions)
    if (this.kickastInstance.options.logOutput) {
      console.log("========= KICKAST GENERATED OUTPUT =========")
      console.log(res)
      console.log("============================================")
    }
    return res
  }

  /**
   * Generate base64 encoded module
   * @param {*} depth 
   */
  toModule(options) {
    let src = this.toString(options)
    let b64moduleData = `data:text/javascript;base64,${Buffer.from(src).toString('base64')}`;
    console.log(b64moduleData)
    return import(b64moduleData)
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Modules API
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   * @param {string} source
   * @returns
   * @memberof Program
   */
  ImportDeclaration(source) {
    return this.import = new Internal.ImportDeclaration(this, source);
  }

  /**
   *
   *
   * @param {*} imported
   * @param {*} local
   * @returns
   * @memberof Program
   */
  ImportSpecifier(imported, local) {
    return this.import = new Internal.ImportSpecifier(this, imported, local);
  }

  /**
   *
   *
   * @param {*} local
   * @returns
   * @memberof Program
   */
  ImportNamedSpecifier(local) {
    return this.import = new Internal.ImportNamedSpecifier(this, local);
  }

  ImportAllDeclaration() {
    return this.import = new Internal.ImportAllDeclaration(this);
  }

  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} source
   * @param {*} exported
   * @returns
   * @memberof Program
   */
  ExportAllDeclaration(source, exported) {
    return this.export = new Internal.ExportAllDeclaration(this, source, exported);
  }

  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   * @memberof Program
   */
  ExportDefaultDeclaration(cb) {
    return this.export = new Internal.ExportDefaultDeclaration(this, cb);
  }

  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @param {string} spec
   * @param {string} src
   * @returns
   * @memberof Program
   */
  ExportNamedDeclaration(cb, spec, src) {
    return this.export = new Internal.ExportNamedDeclaration(this, cb, spec, src);
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Modules API Aliases
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   * @param {string=} source
   * @returns
   * @memberof Program
   */
  Import(source) {
    return this.ImportDeclaration(source);
  }

  ImportAll() {
    return this.ImportAllDeclaration();
  }

  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   * @memberof Program
   */
  ExportDefault(cb) {
    return this.ExportDefaultDeclaration(cb);
  }

  ExportAll() {
    return this.ExportAllDeclaration();
  }

  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @param {string} spec
   * @param {string} src
   * @returns
   * @memberof Program
   */
  Export(cb, spec, src) {
    return this.ExportNamedDeclaration(cb, spec, src);
  }
}

export default Program;