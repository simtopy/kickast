import * as Internal from '../internal.js';

export default class RawFunctionExpression extends Internal.ConcreteExpression {
  constructor(node, f) {
    super(node)
    let comments = []
    let str = f.toString().replace("function anonymous", "function")
    let ast = Internal.parse(`(${str})`, {
      onComment: function(block, text, start, end) {
        comments.push({
          type: block ? "Block" : "Line",
          value: text,
          start,
          end
        })
      }
    })
    this.function = ast.body[0].expression;
    this.function.body.comments = comments
    if (this.function.type != "FunctionExpression") {
      throw Error("argument should be a function expression")
    }
  }

  get _node() {
    return this.function
  }
};