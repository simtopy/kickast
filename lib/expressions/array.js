import * as Internal from '../internal.js';

export default class ArrayExpression extends Internal.IdentifierLike {

  constructor(node, elements) {
    super(node)
    this.elements = []
    if (typeof elements === "function") this.apply(elements, this)
    else if (Array.isArray(elements)) {
      this.elements = elements.map(e => new Internal.Literal(this, e))
    }
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      elements: this.elements.map(e => e.node)
    }
  }
  /* ArrayExpression API */

  /**
   *
   *
   * @param {string[]} keys
   * @returns
   */
  Identifiers(...keys) {
    this.elements.push(...keys.map(key => new Internal.Identifier(this, key)))
    return this
  }

  /**
   *
   *
   * @param {Internal.primitives[]} array
   * @returns
   */
  Literals(...array) {
    this.elements.push(...array.map(e => {
      return new Internal.Literal(this, e)
    }))
    return this
  }

  /**
   *
   *
   * @param {string} array
   * @returns
   */
  Expressions(...array) {
    this.elements.push(...array.map(callback => {
      return this.apply(callback, new Internal.PlaceholderExpression(this))
    }))
    return this
  }

  /**
   * Array transformation
   * 
   * @param {string} array
   * @param {Internal.ExpressionGeneratorCb} callback
   * @returns
   */
  From(array, callback) {
    this.elements.push(...array.map((data, index) => {
      return this.apply(callback, new Internal.PlaceholderExpression(this), data, index)
    }))
    return this
  }

};