import * as Internal from '../internal.js';

export default class MemberExpression extends Internal.Identifier {
  constructor(node, object, property, optional = false) {
    super(node);
    this.object = object;
    // Compute property
    if (typeof property === "function") {
      this.computed = true;
      this.property = this.apply(property, new Internal.PlaceholderExpression(this))
    } else if (Number.isInteger(property)) {
      this.computed = true;
      this.property = new Internal.Literal(this, property);
    } else {
      this.computed = false;
      this.property = new Internal.Identifier(this, property);
    }

    this.optional = optional

  }

  get _debugOutput() {
    return `ptional: ${this.optional}`
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      object: this.object.node,
      property: this.property.node,
      computed: this.computed,
      optional: this.optional
    }
  }

  set expression(e) {
    return this.trueParent.expression = e;
  }

  get expression() {
    return this
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Members API
  ///////////////////////////////////////////////////////////////////////////////
};