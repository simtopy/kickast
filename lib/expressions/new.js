import * as Internal from '../internal.js';
const unchainable = ["Literal"];

export default class NewExpression extends Internal.IdentifierLike {
  constructor(node, className, ...args) {
    super(node, null, unchainable);
    this.callee = new Internal.Identifier(this, className);
    this.arguments = args.map(arg => {
      if (typeof arg === "function") return this.apply(arg, new Internal.PlaceholderExpression(this))
      else return new Internal.Literal(this, arg);
    });
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      callee: this.callee.node,
      arguments: this.arguments.map(e => e.node)
    }
  }
};