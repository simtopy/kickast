import * as Internal from '../internal.js';

export default class AwaitExpression extends Internal.TargetableExpression {
  constructor(node) {
    super(node);
    this.argument = null;
  }

  set expression(e) {
    this.argument = e;
    return e;
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      argument: this.argument.node,
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // Promises
  /////////////////////////////////////////////////////////////////////////////

  /*
    Promise.resolve(<...expressions>)
  */

  /**
   *
   * @param {Internal.ExpressionGeneratorCb | string} args - expression or raw value
   * @returns {Internal.CallExpression}
   */
  Resolved(...args) {
    return this.Identifier("Promise", "resolve").Call(...args);
  }

  /**
   *
   * @param {Internal.ExpressionGeneratorCb | string} args - expression or raw value
   * @returns {Internal.CallExpression}
   */
  Rejected(...args) {
    return this.Identifier("Promise", "reject").Call(...args);
  }

  /**
   *
   * @param {Internal.ExpressionGeneratorCb | string} args - expression or raw value
   * @returns {Internal.CallExpression}
   */
  PromiseAll(...args) {
    return this.Identifier("Promise", "all").Call(...args)
  }

};