import * as Internal from '../internal.js';

export default class ConditionalExpression extends Internal.LiteralLike {
  constructor(node, test, consequent, alternate) {
    super(node)
    // Test
    if (typeof test === "function") {
      this.test = this.apply(test, new Internal.PlaceholderExpression(this))
    } else this.test = new Internal.Literal(this, test)
    // Consequent
    if (typeof consequent === "function") {
      this.consequent = this.apply(consequent, new Internal.PlaceholderExpression(this))
    } else this.consequent = new Internal.Literal(this, consequent)
    // Alternate
    if (typeof alternate === "function") {
      this.alternate = this.apply(alternate, new Internal.PlaceholderExpression(this))
    } else this.alternate = new Internal.Literal(this, alternate)
  }

  get _node() {
    return {
      type: this.constructor.name,
      test: this.test.node,
      consequent: this.consequent.node,
      alternate: this.alternate.node
    }
  }
};