import * as Internal from '../internal.js';

export default class FunctionExpression extends Internal.LiteralLike {
  constructor(node, async, params, body) {
    super(node)
    this.function = new Internal.Function(node, async, null, params, body)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      expression: false,
      ...this.function.node
    }
  }
};