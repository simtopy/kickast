import * as Internal from '../internal.js';

/**
 * A Template Element
 * 
 * @see{@link https://github.com/estree/estree/blob/master/es2015.md#template-element}
 */
class TemplateElement extends Internal.Node {
  /**
   *Creates an instance of TemplateElement.
   * @param {Internal.Node} parentNode
   * @param {string} content
   * @memberof TemplateElement
   */
  constructor(parentNode, content) {
    super(parentNode)
    this.raw = content
    this.tail = false
  }
  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      value: {
        raw: this.raw,
        tail: this.tail
      },
    }
  }
}

/**
 * A Template Literal
 * 
 * @see{@link https://github.com/estree/estree/blob/master/es2015.md#template-literals}
 */
class TemplateLiteral extends Internal.LiteralLike {
  constructor(parentNode) {
    super(parentNode, null)
    this.quasis = []
    this.expressions = []
  }
  get _node() {
    return {
      type: this.constructor.name,
      quasis: this._sanitizeQuasis(),

      expressions: this.expressions.map(e => e.node)
    }
  }

  /**
   * @private
   *
   * @returns
   * @memberof TemplateLiteral
   */
  _sanitizeQuasis() {
    //for order consistency and empty case
    while (this.quasis.length <= this.expressions.length)
      this.quasis.push(new TemplateElement(this, ""))
    //correctly set the tail
    this.quasis[this.quasis.length - 1].tail = true
    return this.quasis.map(a => a.node)
  }
  /////////////////////////////////////////////////////////////////////////////
  // Template Literal API
  /////////////////////////////////////////////////////////////////////////////

  /**
   * Add a Template Element to the Template Literal (string fragment)
   *
   * @param {string} text
   * @returns {TemplateLiteral}
   * @memberof TemplateLiteral
   */
  Element(text) {
    this.quasis.push(new TemplateElement(this, text))
    return this
  }

  /**
   * Add an expression to evaluate
   *
   * @param {Internal.ExpressionGeneratorCb} cb
   * @memberof TemplateLiteral
   */
  Expression(cb) {
    //check for order consistency
    if (this.quasis.length < this.expressions.length + 1) this.quasis.push(new TemplateElement(this, ""))
    this.expressions.push(cb(new Internal.PlaceholderExpression(this)))
    return this

  }

  /////////////////////////////////////////////////////////////////////////////
  // Template Literal Aliases
  /////////////////////////////////////////////////////////////////////////////

  /**
   * Add a string fragment
   *
   * @param {string} text
   * @memberof TemplateLiteral
   */
  Part(text) {
    return this.Element(text)
  }

  /**
   * Add an Identifier to expand
   *
   * @param {string} name
   * @param {string[]} members 
   * @returns TemplateLiteral
   * @memberof TemplateLiteral
   */
  Id(name, ...members) {
    return this.Expression(e => e.Identifier(name, ...members))
  }

  /**
   * Nested TL 
   *
   * @param {Internal.TemplateLiteralGeneratorCb} cb
   * @returns {Internal.TemplateLiteral}
   * @memberof ExpressionContainer
   */
  TL(cb) {
    return this.Expression(e => TemplateLiteral(cb))
  }
}

export default TemplateLiteral;