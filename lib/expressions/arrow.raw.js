import * as Internal from '../internal.js';

export default class RawArrowFunctionExpression extends Internal.ConcreteExpression {
  constructor(node, f) {
    super(node)
    let comments = []
    let ast = Internal.parse(`(${f.toString()})`, {
      onComment: comments
    })
    this.arrow = ast.body[0].expression
    this.arrow.body.comments = comments
    if (this.arrow.type != "ArrowFunctionExpression") {
      throw Error("argument should be an arrow expression")
    }
  }

  get _node() {
    return this.arrow
  }
};