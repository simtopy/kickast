import * as Internal from '../internal.js';

/**
 * A Placeholder Expression is a temporary undefined expression provided with Expression creation API
 * Therefore, it can be chained wich any Expression Factory method, excluding expression that should
 * only appears in other expression context.
 * 
 * @class PlaceholderExpression
 * @extends {Internal.ExpressionContainer}
 *
 * @example Expression(e => e.Literal("Bonjour"))
 *
 *
 **/
class PlaceholderExpression extends Internal.ExpressionContainer {
  constructor(parentNode) {
    super(parentNode)
    this._expression = null
  }

  //Placeholders Skip themselbes
  get _node() {
    if (!this.expression) {
      throw Error(`A placeholder of type ${this.constructor.name} is remaining, child of node of type ${this.trueParent.constructor.name}. Therefore, I cannot generate a valid AST.`)
    }
    return this.expression.node
  }

  get expression() {
    return this._expression
  }

  set expression(e) {
    this._expression = e
    return e
  }
}
export default PlaceholderExpression;