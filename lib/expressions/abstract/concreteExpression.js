import * as Internal from '../../internal.js';

/**
 * Any Expression provided with some specific manipulation API. These should eventually be
 * dispatched into the final classes
 * 
 * @class ConcreteExpression
 * @extends {Internal.Expression}
 * 
 **/
class ConcreteExpression extends Internal.Expression {
  constructor(parentNode) {
    super(parentNode)
    this._expression = this
    this.comments = null
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
    }
  }

  get expression() {
    return this._expression
  }

  set expression(e) {
    this._expression = e
    return e
  }

  //JS DOC TYPE ANNOTATION
  get jsDocType() {
    return this._expression._typeAnnotation
  }
  set jsDocType(type) {
    if (type === null) {
      this._expression._typeAnnotation = (new PlaceholderType)
      this._expression._typeAnnotation.Any
    }
    this._expression._typeAnnotation = type
  }

  /**
   * 
   * @param {Internal.typeGeneratorCb} typeGeneratorCb 
   */
  Type(typeGeneratorCb) {
    this.jsDocType = new Internal.PlaceholderType()
    typeGeneratorCb(this.jsDocType)
    return this
  }

}
export default ConcreteExpression;