import * as Internal from '../../internal.js'

/**
 * An expression that can contains another expression (generally abstract)
 *
 * @class ExpressionContainer
 * @extends {Internal.Expression}
 **/
class ExpressionContainer extends Internal.Expression {
  constructor(parentNode) {
    super(parentNode)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
    }
  }

  get expression() {
    return this._expression
  }

  set expression(e) {
    throw Error("Shall be overriden")
    this._expression = e
    return e
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Expression API
  ///////////////////////////////////////////////////////////////////////////////

  /**
   * 
   * @param {Internal.primitives} value - string representation of the value or any value that map to this representation
   * @returns
   * @memberof ExpressionContainer
   */
  Literal(value) {
    return this.expression = new Internal.Literal(this, value);
  }

  /**
   *
   * @param {string} name
   * @returns
   * @memberof ExpressionContainer
   */
  IdentifierExpression(name) {
    return this.expression = new Internal.Identifier(this, name);
  }

  /**
   *
   *
   * @param {Internal.ObjectGeneratorCb=} callback
   * @returns {Internal.ObjectExpression}
   * @memberof ExpressionContainer
   */
  ObjectExpression(callback) {
    return this.expression = new Internal.ObjectExpression(this, callback);
  }

  /**
   *
   *
   * @param {Internal.ArrayGeneratorCb=} callback
   * @returns {Internal.ArrayExpression}
   * @memberof ExpressionContainer
   */
  Array(callback) {
    return this.expression = new Internal.ArrayExpression(this, callback);
  }

  /**
   *
   *
   * @param {Function} f - an arrow function to parse and embed in the ast
   * @returns
   * @memberof ExpressionContainer
   */
  RawArrow(f) {
    return this.expression = new Internal.RawArrowFunctionExpression(this, f);
  }

  /**
   * 
   *
   * @param {Function} f - a function to parse and embed in the ast
   * @returns
   * @memberof ExpressionContainer
   */
  RawFunction(f) {
    return this.expression = new Internal.RawFunctionExpression(this, f);
  }

  /**
   *
   *
   * @param {boolean} async - is the function asynchrone
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof ExpressionContainer
   */
  FunctionExpression(async, params, body) {
    return this.expression = new Internal.FunctionExpression(this, async, params, body);
  }

  /**
   *
   *
   * @param {boolean} async
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} [body=null]
   * @returns
   * @memberof ExpressionContainer
   */
  ArrowFunctionExpression(async, params, body = null) {
    return this.expression = new Internal.ArrowFunctionExpression(this, async, params, body);
  }

  Await() {
    return this.expression = new Internal.AwaitExpression(this);
  }

  /**
   *
   *
   * @param {string} callee
   * @param {Internal.StatementGeneratorCb | string[]} args - expression or literal value
   * @returns {Internal.CallExpression}
   * @memberof ExpressionContainer
   */
  CallExpression(callee, ...args) {
    return this.expression = new Internal.CallExpression(this, callee, optional = false, ...args);
  }

  /**
   *
   * 
   * @param {string} className
   * @param {Internal.StatementGeneratorCb[] | string[]} args - expression or literal value
   * @returns {Internal.NewExpression}
   * @memberof ExpressionContainer
   */
  New(className, ...args) {
    return this.expression = new Internal.NewExpression(this, className, ...args);
  }

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb} argument
   * @returns {Internal.Spread}
   * @memberof ExpressionContainer
   */
  SpreadElement(argument) {
    return this.expression = new Internal.SpreadElement(this, argument);
  }

  /**
   *
   * @param {any} argument
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   * 
   * Note : don't know if this is a mistake or intentional - Lucas
   */
  Spread() {
    return this.SpreadElement().expression
  }

  /**
   *
   *
   * @param {string} operator
   * @param {Internal.StatementGeneratorCb} argument
   * @param {boolean} [prefix=true]
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  UnaryExpression(operator, argument, prefix = true) {
    return this.expression = new Internal.UnaryExpression(this, operator, argument, prefix);
  }

  /**
   *
   * @private
   * @param {string} operator
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  UnaryInline(operator) {
    return this.UnaryExpression(operator).argument;
  }

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb} left
   * @param {string} operator
   * @param {Internal.ExpressionGeneratorCb} right
   * @returns
   * @memberof ExpressionContainer
   */
  BinaryExpression(left, operator, right) {
    return this.expression = new Internal.BinaryExpression(this, left, operator, right);
  }

  /**
   *
   * @param {Internal.ExpressionGeneratorCb} left
   * @param {string} operator
   * @param {Internal.ExpressionGeneratorCb} right
   * @returns
   * @memberof ExpressionContainer
   */
  LogicalExpression(left, operator, right) {
    return this.expression = new Internal.LogicalExpression(this, left, operator, right);
  }

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb | string } test callback or literal value 
   * @param {Internal.ExpressionGeneratorCb | string } consequent callback or literal value
   * @param {Internal.ExpressionGeneratorCb | string } alternate callback or literal value
   * @returns
   * @memberof ExpressionContainer
   */
  Ternary(test, consequent, alternate) {
    return this.expression = new Internal.ConditionalExpression(this, test, consequent, alternate);
  }

  /**
   *
   * @param {Internal.ExpressionGeneratorCb} left
   * @param {string} operator
   * @param {Internal.ExpressionGeneratorCb} right
   * @returns
   * @memberof ExpressionContainer
   */
  AssignmentExpression(left, operator, right) {
    return this.expression = new Internal.AssignmentExpression(this, left, operator, right);
  }

  /**
   *
   *
   * @param {Internal.Node} node
   * @param {Internal.ExpressionGeneratorCb[]} expressions
   * @returns
   * @memberof ExpressionContainer
   */
  SequenceExpression(expressions) {
    return this.expression = new Internal.SequenceExpression(this, expressions);
  }

  /**
   *
   * Build a template string, providing an optional callback or using the method api
   * 
   * @param {Internal.IdentifierLike} tag
   * @param {Internal.TemplateLiteral} quasi
   * @returns {Internal.TemplateLiteral}
   * @memberof ExpressionContainer
   */
  TaggedTemplateExpression(tag, quasi) {
    let ttl = new Internal.TaggedTemplateExpression(this, tag, quasi)
    return this.expression = ttl
  }

  /**
   *
   * Build a template string, providing an optional callback or using the method api
   * 
   * @param {Internal.TemplateLiteralGeneratorCb} cb
   * @returns {Internal.TemplateLiteral}
   * @memberof ExpressionContainer
   */
  TemplateLiteral(cb) {
    let tl = new Internal.TemplateLiteral(this)
    if (cb) cb(tl)
    return this.expression = tl
  }

  /**
   * 
   * @param {string} name
   * @param {Internal.ClassGeneratorCb} callback
   * @returns
   * @memberof ExpressionContainer
   */
  ClassDeclaration(name, callback) {
    return this.expression = new Internal.ClassDeclaration(this, name, callback)
  }

  ///////////////////////////////////////////////////////////////////////////////
  // General Aliases
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @param {string} str
   * @returns
   * @memberof ExpressionContainer
   */
  Raw(str) {
    //console.trace("Raw Expression", str);
    return this.Identifier(str);
  }

  Bracket(propertyName) {
    return this.MemberInline(e => e.Literal(propertyName));
  }

  /**
   * @param {Internal.ObjectGeneratorCb=} cb
   * @returns 
   * @memberof ExpressionContainer
   */
  Object(cb) {
    return this.ObjectExpression(cb);
  }

  /**
   *
   *
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof ExpressionContainer
   */
  Function(params, body) {
    return this.FunctionExpression(false, params, body);
  }

  Class(name, callback) {
    return this.ClassDeclaration(name, callback)
  }

  /**
   *
   *
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns {Internal.FunctionExpression}
   * @memberof ExpressionContainer
   */
  AsyncFunction(params, body) {
    return this.FunctionExpression(true, params, body);
  }

  /**
   *
   *
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  Arrow(params, body) {
    return this.ArrowFunctionExpression(false, params, body).function.body;
  }

  /**
   *
   *
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  AsyncArrow(params, body) {
    return this.ArrowFunctionExpression(true, params, body).function.body;
  }

  /**
   *
   * @param {Internal.StatementGeneratorCb} body
   * @returns {Internal.CallExpression}
   * @memberof ExpressionContainer
   */
  AsyncPromise(body) {
    return this.Identifier("Promise")
      .Call(e => e.AsyncArrow({
        "resolve": "function",
        "reject": "function"
      }).Body(body));
  }

  /**
   *
   *
   * @param {Internal.paramsGenerator} args
   * @returns
   * @memberof ExpressionContainer
   */
  Actions(...args) {
    const obj = args.pop()
    return this.This("props", "Actions").Call(...args, e => e.Object(obj))
  }

  /**
   * 
   *
   * @param {string | Internal.ExpressionGeneratorCb} obj
   * @returns
   * @memberof ExpressionContainer
   * 
   * 
   */
  Dispatch(obj) {
    return this.This('props', 'dispatch').Call(e => e.Object(o => obj(o)))
  }

  /**
   *
   * @param {String} key
   * @param {String[]} members
   * @returns
   * @memberof ExpressionContainer
   * 
   * @deprecated Use Id instead
   */
  Get(key, ...members) {
    return this.Identifier(key).Members(...members);
  }

  /**
   *
   *
   * @param {String} key
   * @param {String[]} members
   * @returns
   * @memberof ExpressionContainer
   * 
   */
  Id(key, ...members) {
    return this.Identifier(key).Members(...members);
  }

  /**
   *
   *
   * @param {String} key
   * @param {String[]} members
   * @returns
   * @memberof ExpressionContainer
   */
  Set(key, ...members) {
    return this.Assignment(e => e.Id(key, ...members), "=", null).right
  }

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb} expressions
   * @returns
   * @memberof ExpressionContainer
   */
  Sequence(...expressions) {
    return this.SequenceExpression(this, expressions);
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Identifiers Aliases 
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @param {string} name
   * @param {string[]} members
   * @returns
   * @memberof ExpressionContainer
   */
  Identifier(name, ...members) {
    return this.IdentifierExpression(name).Members(...members);
  }

  Undefined() {
    return this.Identifier("undefined");
  }

  /**
   *
   * @param {string[]} members
   * @returns
   * @memberof ExpressionContainer
   */
  This(...members) {
    return this.Identifier("this", ...members);
  }

  /**
   * 
   * @param  {string[]} members
   * @returns
   */
  Super(...members) {
    return this.Identifier("super", ...members);
  }

  /**
   *
   *
   * @param {string[] | Internal.ExpressionGeneratorCb[]} args
   * @returns
   * @memberof ExpressionContainer
   */
  ObjectKeys(...args) {
    return this.Identifier("Object", "keys").Call(...args);
  }

  /**
   *
   *
   * @param {string[] | Internal.ExpressionGeneratorCb[]} args
   * @returns
   * @memberof ExpressionContainer
   */
  ObjectAssign(...args) {
    return this.Identifier("Object", "assign").Call(...args);
  }

  /**
   *
   *
   * @param {string[] | Internal.ExpressionGeneratorCb[]} args
   * @returns
   * @memberof ExpressionContainer
   */
  ObjectEntries(...args) {
    return this.Identifier("Object", "entries").Call(...args);
  }

  /**
   *
   *
   * @param {string[] | Internal.ExpressionGeneratorCb[]} args
   * @returns
   * @memberof ExpressionContainer
   */
  ObjectValues(...args) {
    return this.Identifier("Object", "values").Call(...args);
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Promises Aliases
  ///////////////////////////////////////////////////////////////////////////////
  /*
    Promise((resolve,reject) => {
     <statements>
    });
  */

  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} statements
   * @returns
   * @memberof ExpressionContainer
   */
  Promise(statements) {
    return this.New("Promise", e => e.Arrow(["resolve", "reject"], statements))
  }

  /*
    Promise.resolve(<...expressions>)
  */

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb[] | string[]} args - Expression or literal value
   * @returns
   * @memberof ExpressionContainer
   */
  Resolved(...args) {
    return this.Identifier("Promise", "resolve").Call(...args);
  }

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb[] | string[]} args  - Expression or literal value
   * @returns
   * @memberof ExpressionContainer
   */
  Rejected(...args) {
    return this.Identifier("Promise", "reject").Call(...args);
  }

  /*
    Promise.all(<...expressions>)
  */

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb[] | string[]} args - Expession or literal value
   * @returns
   * @memberof ExpressionContainer
   */
  PromiseAll(...args) {
    return this.Identifier("Promise", "all").Call(...args)
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Assignement API Aliases 
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb} left
   * @param {string} operator
   * @param {Internal.ExpressionGeneratorCb} right
   * @returns
   * @memberof ExpressionContainer
   */
  Assignment(left, operator, right) {
    return this.AssignmentExpression(left, operator, right);
  }

  /**
   *
   * @param {string} operator
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  AssignmentInline(operator) {
    return this.AssignmentExpression(this.concreteParent, this, operator, null).right;
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  Assign() {
    return this.AssignmentInline("=");
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Unary Unary Operators Aliases
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  opp() {
    return this.UnaryInline("-");
  }

  /**
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  Not() {
    return this.UnaryInline("!");
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Literal Aliases 
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   * @returns {Internal.Literal}
   * @memberof ExpressionContainer
   */
  True() {
    return this.Literal(true);
  }

  /**
   *
   *
   * @returns {Internal.Literal}
   * @memberof ExpressionContainer
   */
  False() {
    return this.Literal(false);
  }

  /**
   *
   *
   * @returns {Internal.Literal}
   * @memberof ExpressionContainer
   */
  Null() {
    return this.Literal(null);
  }

  Void() {
    return this.Literal("void")
  }

  /**
   *
   *
   * @param {Internal.primitives} v
   * @returns {Internal.Literal}
   * @memberof ExpressionContainer
   */
  Value(v) {
    return this.Literal(v);
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Binary Operators Aliases 
  ///////////////////////////////////////////////////////////////////////////////
  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb} left
   * @param {string} operator
   * @param {Internal.ExpressionGeneratorCb} right
   * @returns
   * @memberof ExpressionContainer
   */
  Binary(left, operator, right) {
    return this.BinaryExpression(left, operator, right);
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Template Literals Aliases
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @param {Internal.TemplateLiteralGeneratorCb} cb
   * @returns {Internal.TemplateLiteral}
   * @memberof ExpressionContainer
   */
  TL(cb) {
    return this.TemplateLiteral(cb)
  }

  // JSDOC OVERRIDES
}

export default ExpressionContainer