import * as Internal from '../../internal.js';

/**
 * Any expression
 *
 * @class Expression
 * @extends {Internal.Node}
 */
class Expression extends Internal.Node {

  constructor(parentNode) {
    super(parentNode)
    this._expression = null
  }

  get _node() {
    return super._node
  }

  get concreteParent() {
    if (this.trueParent === null) return null
    if (this.trueParent instanceof Internal.PlaceholderExpression) return this.trueParent.concreteParent
    else return this.trueParent
  }

  //JS DOC TYPE ANNOTATION
  get jsDocType() {
    if (!this._expression) return null
    return this._expression._typeAnnotation
  }
  set jsDocType(type) {
    if (type === null) {
      this._expression._typeAnnotation = (new Internal.PlaceholderType())
      this._expression._typeAnnotation.Any
    }
    this._expression._typeAnnotation = type
  }

  /**
   * 
   * @param {Internal.typeGeneratorCb} typeGeneratorCb 
   */
  Type(typeGeneratorCb) {
    this.jsDocType = new Internal.PlaceholderType()
    typeGeneratorCb(this.jsDocType)
    return this
  }

}
export default Expression;