import * as Internal from '../../internal.js';

/**
 * Expression that can be manipulated as a Identifier
 * (eg : a function call) 
 * 
 * @class IdentifierLike
 * @extends {Internal.LiteralLike}
 * 
 **/
class IdentifierLike extends Internal.LiteralLike {
  constructor(node) {
    super(node)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      name: this.name,
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // IdentifierLike
  /////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb[] | string[] } args - expression or raw value 
   * @param {boolean} optional - Is the function call optional 
   * @returns
   * @memberof IdentifierLike
   */
  Call(...args) {
    return this.trueParent.expression = new Internal.CallExpression(this.trueParent, this, false, ...args);
  }

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb[] | string[] } args - expression or raw value 
   * @param {boolean} optional - Is the function call optional 
   * @returns
   * @memberof IdentifierLike
   */
  OptionalCall(...args) {
    return this.trueParent.expression = new Internal.CallExpression(this.trueParent, this, true, ...args);
  }

  /**
   *
   *
   * @param {Internal.Node} node
   * @param {IdentifierLike} object
   * @param {Internal.ExpressionGeneratorCb | number | string } property
   * @param {boolean} optional - Is the chainElement optional
   * @returns
   * @memberof IdentifierLike
   */
  MemberExpression(node, object, property, optional = false) {
    return node.expression = new Internal.MemberExpression(node, object, property, optional);
  }

  /**
   *
   * @param {Internal.ExpressionGeneratorCb | number | string } property
   * @param {boolean} optional - Is the chainElement optional
   * @returns
   * @memberof IdentifierLike
   */
  MemberInline(property, optional = false) {
    return this.MemberExpression(this.trueParent, this, property, optional);
  }

  /**
   * 
   * @param {Internal.ExpressionGeneratorCb | number | string} property
   * @param {boolean} optional - Is this chainElement optional
   * @returns
   * @memberof IdentifierLike
   */
  Member(property, optional = false) {
    return this.MemberInline(property, optional);
  }

  /**
   * An optional member
   * 
   * @param {Internal.ExpressionGeneratorCb | number | string} property 
   * @returns 
   */
  OptionalMember(property) {
    return this.Member(property, true)
  }

  /**
   * 
   * @param  {...string} array 
   * @param {boolean} optional - Are the chainElement optional 
   */
  Members(...array) {
    if (array.length === 0) return this;
    let e = this.Member(array.shift())
    //console.log("members", array, e)
    while (array.length > 0) {
      e = e.Member(array.shift(), false);
    }
    return e;
  }

  /**
   * 
   * @param  {...string} array 
   * @param {boolean} optional - Are the chainElements optional 
   */
  OptionalMembers(...array) {
    if (array.length === 0) return this;
    let e = this.Member(array.shift())
    //console.log("members", array, e)
    while (array.length > 0) {
      e = e.Member(array.shift(), true);
    }
    return e;
  }

  //////////////////////////////////////////////////////////////////////////////
  // Array API Aliases
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb[] | string[]} args
   * @returns
   * @memberof IdentifierLike
   */
  Push(...args) {
    return this.Member("push").Call(...args);
  }

  //Todo
  Map(cb) {
    return this.Member("map").Call(cb)
  }

  Find(cb) {
    return this.Member("find").Call(cb);
  }

  Filter(cb) {
    return this.Member("filter").Call(cb);
  }

  ForEach(cb) {
    return this.Member("forEach").Call(cb);
  }

  Includes(cb) {
    return this.Member("includes").Call(cb);
  }

  Reduce(cb, init) {
    return this.Member("reduce").Call(cb, init);
  }

  Index(...indices) {
    return this.Members(...indices.map(i => {
      if (typeof i === "number") return e => e.Value(i)
      else if (typeof i === "string") return e => e.Identifier(i)
      else if (typeof i === "function") return i;
      else throw Error(`index ${i} should be number, string or function`);
    }));
  }

  /**
   * 
   * @param {String} com 
   */
  Describe(com) {
    this.jsDocComment = com
    return this
  }
}

export default IdentifierLike;