import * as Internal from '../../internal.js';
/**
 * An Expression that can contains a Identifier subexpression
 *
 * @class TargetableExpression
 * @extends {Internal.ConcreteExpression}
 **/
class TargetableExpression extends Internal.ConcreteExpression {
  constructor(parentNode) {
    super(parentNode)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
    }
  }

  /**
   *
   *
   * @param {string} name
   * @returns {Internal.Identifier}
   * @memberof TargetableExpression
   */
  IdentifierExpression(name) {
    return this.expression = new Internal.Identifier(this, name);
  }

  /**
   *
   *
   * @param {string} name
   * @param {string[]} members
   * @returns {Internal.Identifier | Internal.MemberExpression}
   * @memberof TargetableExpression
   */
  Identifier(name, ...members) {
    return this.IdentifierExpression(name).Members(...members);
  }

  /**
   *
   * @deprecated use Id() instead
   * @param {string} key
   * @param {string[]} members
   * @returns {Internal.Identifier | Internal.MemberExpression}
   * @memberof TargetableExpression
   */
  Get(key, ...members) {
    return this.Identifier(key).Members(...members);
  }

  /**
   *
   * @deprecated use Id() instead
   * @param {string} key
   * @param {string[]} members
   * @returns {Internal.Identifier | Internal.MemberExpressions}
   * @memberof TargetableExpression
   */
  Id(key, ...members) {
    return this.Identifier(key).Members(...members);
  }

  /**
   *
   *
   * @param {string[]} members
   * @returns {Internal.Identifier | Internal.MemberExpression}
   * @memberof TargetableExpression
   */
  This(...members) {
    return this.Identifier("this", ...members);
  }

  /**
   *
   * 
   * @param {string} className
   * @param {Internal.StatementGeneratorCb[] | string[]} args - expression or literal value
   * @returns {Internal.NewExpression}
   * @memberof ExpressionContainer
   */
  New(className, ...args) {
    return this.expression = new Internal.NewExpression(this, className, ...args);
  }

}
export default TargetableExpression;