import * as Internal from '../../internal.js';

const chainable = null;
const unchainable = ["Literal"];

/**
 * An expression that can be manipulated as a litteral
 *
 * @class LiteralLike
 * @extends {Internal.ConcreteExpression}
 */
class LiteralLike extends Internal.ConcreteExpression {
  constructor(node) {
    super(node);
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
    }
  }

  /**
   *
   * @param {Internal.ExpressionGeneratorCb | number | string } property
   * @returns
   * @memberof LiteralLike
   */
  TaggedTemplate(cb) {
    let tl = new Internal.TemplateLiteral(this)
    if (cb) cb(tl)
    return this.TaggedTemplateLiteralExpression(this.trueParent, tl);
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Binary Operators Aliases 
  ///////////////////////////////////////////////////////////////////////////////

  /**
   *
   * @private
   * @param {string} operator
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  UnaryInline(operator) {
    return this._UnaryExpression(operator).argument;
  }

  /**
   *
   *
   * @param {string} operator
   * @param {Internal.StatementGeneratorCb} argument
   * @param {boolean} [prefix=true]
   * @returns {Internal.PlaceholderExpression}
   * @memberof ExpressionContainer
   */
  _UnaryExpression(operator, argument, prefix = true) {
    return this.trueParent.expression = new Internal.UnaryExpression(this.trueParent, operator, argument, prefix);
  }

  /**
   *
   *
   * @param {Internal.Node} node
   * @param {Internal.PlaceholderExpression} left
   * @param {string} operator
   * @param {Internal.PlaceholderExpression} right
   * @returns
   * @memberof LiteralLike
   * @private
   */
  _LogicalExpression(operator, right) {
    return this.trueParent.expression = new Internal.LogicalExpression(this.trueParent, this.expression, operator, right);
  }

  /**
   * @private
   * @param {*} node 
   * @param {*} left 
   * @param {*} operator 
   * @param {*} right 
   * @returns 
   */
  _BinaryExpression(operator, right) {
    const expr = new Internal.BinaryExpression(this.trueParent, this.expression, operator, right);
    this.trueParent.expression = expr
    return expr
  }
  /**
   *
   * @param {string} operator
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  BinaryInline(operator) {
    return this._BinaryExpression(operator, null).right
  }

  /**
   *
   *
   * @param {string} operator
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  LogicalInline(operator) {
    return this._LogicalExpression(operator, null).right
  }

  /**
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  Instanceof() {
    return this.BinaryInline("instanceof");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  In() {
    return this.BinaryInline("in");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  Equals() {
    return this.BinaryInline("===");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  eq() {
    return this.Equals();
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  NotEquals() {
    return this.BinaryInline("!==");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  ne() {
    return this.NotEquals();
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  And() {
    return this.LogicalInline("&&");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  Or() {
    return this.LogicalInline("||");
  }

  /**
   * Null coalescing operator
   * @returns {Internal.PlaceholderExpression}
   */
  NullCoalescing() {
    return this.LogicalInline("??")
  }

  /**
   * Null coalescing operator
   * @returns {Internal.PlaceholderExpression}
   */
  NotNullOr() {
    return this.NullCoalescing()
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  Greater() {
    return this.BinaryInline(">");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  gt() {
    return this.Greater();
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  ge() {
    return this.BinaryInline(">=");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  Less() {
    return this.BinaryInline("<");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  Lower() {
    return this.Less();
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  lt() {
    return this.Less();
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  le() {
    return this.BinaryInline("<=");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  p() { // plus
    return this.BinaryInline("+");
  }

  /**
   *
   * @returns
   * @memberof LiteralLike
   */
  pow() {
    return this.BinaryInline("**")
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  plus() {
    return this.p();
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  m() { // minus
    return this.BinaryInline("-");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  minus() {
    return this.m();
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  x() { // times
    return this.BinaryInline("*");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  times() {
    return this.x();
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  d() { // divide
    return this.BinaryInline("/");
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  div() {
    return this.d();
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof LiteralLike
   */
  dot() { // dot operator (to force dot in Python)
    return this.BinaryInline(".");
  }

}

export default LiteralLike;