import * as Internal from '../internal.js';
//const chainable = ["Array", "Get" "Identifier"]
const chainable = null
const unchainable = []

export default class SpreadElement extends Internal.Node {

  constructor(parentNode, argument, coms) {
    super(parentNode)
    this.argument = this.apply(argument, new Internal.PlaceholderExpression(this))
    //this.comments = []
    //if (coms) this.comment(coms)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      argument: this.argument.node,
      //comments: this.comments,
    }
  }

  get expression() {
    return this.argument
  }

  set expression(e) {
    this.argument = e
    return e
  }
};