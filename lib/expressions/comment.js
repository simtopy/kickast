import Expression from './expression';

export default class CommentExpression extends Expression {
  constructor(node, comment, tabs = 1) {
    super(node);

    this.tabs = tabs;
    this.comment = comment;
  }
}