import * as Internal from '../internal.js';

/**
 * ES5 Identifier
 * @class
 * @extends {Internal.IdentifierLike}
 *
 * @see https://github.com/estree/estree/blob/master/es5.md#identifier
 **/
class Identifier extends Internal.IdentifierLike {
  constructor(node, name) {
    super(node)
    this.name = name
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      name: this.name,
    }
  }

  get _debugOutput() {
    return `${this.name}`
  }


  /////////////////////////////////////////////////////////////////////////////
  // Identifier
  /////////////////////////////////////////////////////////////////////////////

  /**
   * @private
   */
  _AssignmentExpression(operator, right) {
    const expr = new Internal.AssignmentExpression(this.trueParent, this.expression, operator, right);
    this.trueParent.expression = expr
    return expr
  }

  /**
   *
   *
   * @returns {Internal.PlaceholderExpression}
   * @memberof Identifier
   */
  Assign() {
    return this.AssignmentInline("=");
  }

  /**
   * Return the right value to be setted inline
   *
   *
   * @param {string} operator - string representation of operator
   * @returns {Internal.PlaceholderExpression}
   * @memberof Identifier
   */
  AssignmentInline(operator) {
    return this._AssignmentExpression(operator, null).right;
  }

  /**
   *
   *
   * @returns {Internal.ExpressionGeneratorCb}
   * @memberof Identifier
   */
  Increment() {
    return this.AssignmentInline("+=");
  }

  /**
   *
   *
   * @returns {Internal.ExpressionGeneratorCb}
   * @memberof Identifier
   */
  Decrement() {
    return this.AssignmentInline("-=");
  }

}

export default Identifier;