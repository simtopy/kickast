import * as Internal from '../internal.js';
export default class ObjectExpression extends Internal.IdentifierLike {

  constructor(node, callback) {
    super(node)
    this.properties = []
    if (callback) this.apply(callback, this)
    // doc handling
    const jsDoc = (jsd) => {
      if (this.jsDocComment !== "") {
        jsd.Raw(this.jsDocComment)
      }
      jsd.Type(t => this.typeName ? t.UserDefinedType(this.typeName) : t.Object, null)
      this.properties.forEach(p => {
        if (p.key) {
          p.jsDocTags = new Internal.JsdocSequence(jsd2 => {
            if (p.jsDocComment) jsd2.Raw(p.jsDocComment)
            if (p.jsDocType) jsd2.Type(p.jsDocType)

            if (p.method) {
              if (p.kind === "get") jsd2.Getter()
              else if (p.kind === "set") jsd2.Setter()
              else jsd2.Method()
            }
          })
        }
      })
    }
    this.autoJSDocIfAllowed(jsDoc)

  }
  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      properties: this.properties.map(prop => prop.node),
    }
  }
  /* Object Expression API */

  /**
   * Give a name to this object
   * 
   * @param {string} name 
   */
  Type(name) {
    this.typeName = name
  }

  /**
   * Static property
   *
   * @param {string} name - Property name
   * @param {(Internal.primitives| Internal.ExpressionGeneratorCb)} init - Initial value
   * @returns
   */
  Static(name, init) {
    const prop = new Internal.Property(this, name, null, "init", false, false, false)
    prop.value = (typeof init === "function") ? this.apply(init, new Internal.PlaceholderExpression(prop)) : new Internal.Literal(prop, init)
    this.properties.push(prop)
    return prop
  }

  /**
   *
   *
   * @param {string} name
   * @param {Internal.StatementGeneratorCb} body
   */
  Getter(name, body) {
    let value = new Internal.FunctionExpression(this, false, [], body)
    const prop = new Internal.Property(this, name, value, "get", false, false, false)
    this.properties.push(prop)
    return prop
  }

  /**
   * Computed propery
   *
   * @param {string} name
   * @param {string | Internal.ExpressionGeneratorCb} init
   * @returns
   */
  Computed(name, init) {
    // value is a expression or a litteral
    let value = (typeof init === "function") ?
      this.apply(init, new Internal.PlaceholderExpression(this)) :
      new Internal.Literal(this, init)
    // create the property
    const prop = new Internal.Property(this, name, value, "init", true, false, false)
    this.properties.push(prop)
    return prop
  }

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb} cb
   * @param {string[]} [coms=null]
   * @returns
   */
  Spread(cb, coms = null) {
    let argument = this.apply(cb, new Internal.PlaceholderExpression(this))
    const prop = new Internal.SpreadElement(this, argument, coms)
    this.properties.push(prop)
    return prop
  }

  /**
   *
   *
   * @param {string} name
   * @param {boolean} [async=false]
   * @param {Internal.paramsGenerator} [params=[]]
   * @param {Internal.StatementGeneratorCb} [body=function() {}] - Function body (Statements) 
   * @param {string[]} [coms=null]
   * @returns
   */
  Method(name, async = false, params = [], body = function () { }) {
    const prop = new Internal.Property(this, name, null, "init", false, true, false)
    prop.value = new Internal.FunctionExpression(prop, async, params, body)

    this.properties.push(prop)
    return prop
  }
  /**
   *
   *
   * @param {string} name
   * @param {function} f - source function
   * @param {string[]} [coms=null]
   * @returns
   */
  RawMethod(name, f) {
    let value = new Internal.RawFunctionExpression(this, f)
    const prop = new Internal.Property(this, name, value, "init", false, true, false)
    this.properties.push(prop)
    return prop
  }

  Shorthand(name) {
    const prop = new Internal.Property(this, name, null, "init", false, false, true)
    this.properties.push(prop)
    return this
  }

  getKey(name) {
    if (typeof name === "string") return {
      key: new Internal.Identifier(this, name),
      computed: false
    }
    if (typeof name === "function") return {
      key: this.apply(name, new Internal.PlaceholderExpression(this)),
      computed: true
    }
    throw Error("parameter name should be a string of a function")
  }

};