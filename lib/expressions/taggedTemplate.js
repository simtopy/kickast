import * as Internal from '../internal.js';

/**
 * A Tagged Template Literal Expression
 * 
 * @see{@link https://github.com/estree/estree/blob/master/es2015.md#taggedtemplateexpression}
 */
class TaggedTemplateExpression extends Internal.LiteralLike {
  constructor(parentNode, tag, quasi) {
    super(parentNode, null)
    this.tag = this.apply(tag, new Internal.PlaceholderExpression(this))
    this.quasi = this.apply(quasi, new Internal.PlaceholderExpression(this))
  }
  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      tag: this.tag.node,
      quasi: this.quasi.node
    }
  }
}

export default TaggedTemplateExpression;