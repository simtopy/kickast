import * as Internal from '../internal.js';
const BinaryOperator = [
  "==", "!=", "===", "!==", "<", "<=", ">", ">=", "<<", ">>", ">>>", "+",
  "-", "*", "/", "%", ",", "^", "&", "in", "instanceof", "&&", "||", "**"
]

export default class BinaryExpression extends Internal.ConcreteExpression {
  /**
   *Creates an instance of BinaryExpression.
   * @param {Internal.Node} node
   * @param {Internal.ExpressionGeneratorCb} left
   * @param {string} operator - a string that represents the binary operator
   * @param {Internal.ExpressionGeneratorCb} right
   */
  constructor(parentNode, left, operator, right) {
    super(parentNode);

    if (!BinaryOperator.includes(operator)) throw Error(`illegal operator ${operator}`);
    else this.operator = operator;
    this.left = this.apply(left, new Internal.PlaceholderExpression(this))
    this.right = this.apply(right, new Internal.PlaceholderExpression(this))
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      operator: this.operator,
      left: this.left.node,
      right: this.right.node
    }
  }

  //set expression(e) {
  //  this.right = e
  //}
};