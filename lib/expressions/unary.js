import * as Internal from '../internal.js';
const UnaryOperator = [
  "-", "+", "!", "~", "typeof", "void", "delete"
]
const chainable = null;
const unchainable = [];

export default class UnaryExpression extends Internal.ConcreteExpression {
  constructor(node, operator, argument = null, prefix = true) {
    super(node, chainable, unchainable);
    // check operator
    if (!UnaryOperator.includes(operator)) throw Error(`illegal operator ${operator}`);
    this.prefix = prefix;
    this.operator = operator;
    this.argument = argument ?
      this.apply(argument, new Internal.PlaceholderExpression(this)) :
      new Internal.PlaceholderExpression(this);
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      prefix: this.prefix,
      operator: this.operator,
      argument: this.argument.node
    }
  }

  get expression() {
    return this.argument;
  }

  set expression(e) {
    this.argument = e;
    return e;
  }
};