import * as Internal from '../internal.js';

export default class ArrowFunctionExpression extends Internal.TargetableExpression {
  constructor(node, async, params, body) {
    super(node);
    this.function = new Internal.Lambda(node, async, params, body);
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      ...this.function.node,
    }
  }
};