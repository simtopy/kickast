import * as Internal from '../internal.js';
const unchainable = ["Literal"];

export default class CallExpression extends Internal.IdentifierLike {
  constructor(parentNode, callee, optional = false, ...args) {
    super(parentNode);
    this.callee = callee;
    this.arguments = args.map(arg => {
      if (typeof arg === "function") return this.apply(arg, new Internal.PlaceholderExpression(this))
      else return new Internal.Literal(this, arg);
    });
    this.optional = optional
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      callee: this.callee.node,
      arguments: this.arguments.map(e => e.node),
      optional: this.optional
    }
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Promise resolution API
  ///////////////////////////////////////////////////////////////////////////////

  Then(...args) {
    return this.Member("then").Call(...args);
  }

  Catch(...args) {
    return this.Member("catch").Call(...args);
  }

};