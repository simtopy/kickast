import * as Internal from '../internal.js';

/**
 * An ES5 Literal
 * @class
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#literal}
 * @extends {Internal.LiteralLike}
 **/
class Literal extends Internal.LiteralLike {
  constructor(node, value) {
    super(node);
    this.value = value;
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      value: this.value
    }
  }

  Type() {
    return typeof this.value;
  }

  get _debugOutput() {
    return `value: ${this.value}`
  }

}

export default Literal;