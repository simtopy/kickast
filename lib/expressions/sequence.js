import * as Internal from '../internal.js';

export default class SequenceExpression extends Internal.ConcreteExpression {

  constructor(node, expressions) {
    super(node);
    this.expressions = expressions.map(
      exp => this.apply(exp, new Internal.PlaceholderExpression(this))
    );
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      expressions: this.expressions.map(exp => exp.node)
    }
  }
};