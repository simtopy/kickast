import * as Internal from '../internal.js';
const LogicalOperator = ["&&", "||", "??"]

export default class LogicalExpression extends Internal.ConcreteExpression {
  constructor(node, left, operator, right) {
    super(node);

    if (!LogicalOperator.includes(operator)) throw Error(`illegal operator ${operator}`);
    else this.operator = operator;
    this.left = this.apply(left, new Internal.PlaceholderExpression(this));
    this.right = this.apply(right, new Internal.PlaceholderExpression(this));
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      operator: this.operator,
      left: this.left.node,
      right: this.right.node
    }
  }

  set expression(e) {
    this.right = e
  }
};