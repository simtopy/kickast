import * as Internal from './internal.js';

/**
 * A JS Function AST representation.
 *
 * @class Function
 * @extends {Internal.Lambda}
 * 
 * @see{@link https://github.com/estree/estree/blob/master/es5.md#functions}
 **/
class Function extends Internal.Lambda {

  constructor(node, async, name, params, body) {
    super(node, async, params, body);
    this.id = name ? new Internal.Identifier(this, name) : null
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      id: this.id ? this.id.node : null,
      async: this.async,
      body: this.body.node,
      params: this.params.map(p => p.node),
    }
  }


  /**
   * Generate a callable function from the AST representation
   *
   * @param {Function} generate - The generator function
   * @param {*} options - Generator options
   * @returns
   */
  toFunction(generate, options) {
    if (this.id) {
      let str = generate({
        type: "FunctionDeclaration",
        ...this.node
      }, options);
      return eval(`${str};${this.id.name}`)
    } else {
      let str = generate({
        type: "FunctionExpression",
        ...this.node
      }, options);
      return eval(`let f = ${str};f`)
    }
  }
}
export default Function;