/* eslint-disable max-classes-per-file */
import * as Internal from './internal.js';

const findReturn = (typeStore, st) => {
  //Return if null (no body)
  if (st === undefined || st === null) return
  // Retrurn statement if found
  if (st instanceof Internal.ReturnStatement) {
    //And is typed
    if (st.argument.jsDocType) typeStore.push(st.argument.jsDocType)
    else {
      typeStore.push(null) // Count non documented return
    }
  } else if (st instanceof Internal.PlaceholderStatement) {
    if (st.statement) findReturn(typeStore, st.statement)
  } else if (st instanceof Internal.BlockStatement) {
    st.body.map(st2 => findReturn(typeStore, st2))
  } else if (st instanceof Internal.IfStatement) {
    findReturn(typeStore, st.consequent)
    if (st.alternate) {
      findReturn(typeStore, st.alternate)
    }
  } else if (st instanceof Internal.WhileStatement || Internal.DoWhileStatement || st instanceof Internal.ForStatement || st instanceof Internal.ForOfStatement || st instanceof Internal.ForInStatement) {
    findReturn(typeStore, st.body)
  } else if (st instanceof Internal.SwitchStatement) {
    st.cases.forEach(c => {
      findReturn(typeStore, c.body)
    })
  }
  return typeStore
}

export class ParameterApi {

  constructor(lambda) {
    this.lambda = lambda
  }
  /**
   *
   * @param {string} name
   * @memberof Lambda
   */
  Id(name) {
    const p = new Internal.Identifier(this.lambda, name)
    this.lambda.params.push(p)
    return p
  }

  /**
   *
   * @param {Internal.ExpressionGeneratorCb | string} name
   * @param {Internal.ExpressionGeneratorCb | string} value
   * @memberof Lambda
   */
  Defaulted(name, value) {
    const p = new Internal.AssignmentPattern(this.lambda, name, value)
    this.lambda.params.push(p)
    return p
  }

  /**
   *
   *
   * @param {Internal.ArrayPatternCb} a
   * @memberof Lambda
   */
  ArrayPattern(a) {
    const p = new Internal.ArrayPattern(this.lambda, a)
    this.lambda.params.push(p)
    return p
  }

  /**
   *
   *
   * @param {Internal.ObjectPatternCb} o
   * @memberof Lambda
   */
  ObjectPattern(o) {
    const p = new Internal.ArrayPattern(this.lambda, o)
    this.lambda.params.push(p)
    return p
  }

  /**
   *
   * @param {string} name
   * @memberof Lambda
   */
  Rest(name) {
    const p = new Internal.RestElement(this.lambda, name)
    this.lambda.params.push(p)
    return p
  }
}

class Lambda extends Internal.VirtualNode {

  /**
   *Creates an instance of Lambda.
   * @param {Internal.Node} node
   * @param {*} async
   * @param {Internal.paramsGenerator} [params=[]]
   * @param {Internal.StatementGeneratorCb} [body=null]
   * @memberof Lambda
   */
  constructor(node, async, params = [], body = null) {
    super(node)
    this.async = !!async
    // generate parameters
    if (params.constructor === Array) {
      this.params = this.array(params).map(p => {
        // identifier
        if (typeof p === "string") {
          return new Internal.Identifier(this, p)
        } else {
          throw Error("string expected")
        }
      })
    } else if (params.constructor === Function) {
      this.params = []
      this.apply(params, new ParameterApi(this))
    } else if (typeof params === "string") {
      this.params = [new Internal.Identifier(this, params)]
    }

    // generate body
    if (body) {
      this.body = new Internal.BlockStatement(this, body)
      this._expression = false
    } else {
      this.body = this.apply(body, new Internal.PlaceholderExpression(this))
      this._expression = true
    }

    //JS DOC //////////////////////////
    const jsDoc = (jsd) => {
      const returnTypeStore = []
      let returnSomething = this._expression || findReturn(returnTypeStore, this.body).length > 0
      if (this.async) {
        jsd.Async()
      }
      if (this.jsDocComment !== "") {
        jsd.Description(this.concreteParent.jsDocComment)
      }
      //Param handling
      this.params.forEach(p => {
        if (p.name) {
          jsd.Param(p.jsDocType, p.name, p.jsDocComment)
        }
      })
      //Return type handling
      if (returnSomething) jsd.Returns(t => {
        t.Union(u => {
          if (!this._expression) {
            if (this.kickastInstance.options.jsDocHints && returnTypeStore.includes(null)) {
              jsd.CustomTag("todo", "", "kickast-hint: you may add a JSDoc type annotation to this return statement")
              t.Any
            } else {
              returnTypeStore.filter(typeAnnotation => typeAnnotation !== null).forEach(typeAnnotation => u.types.push(typeAnnotation))
            }
          }
          // If the body is an expression
          else
            /* beautify preserve:start */
            if (this.body.jsDocType) u.types.push(this.body.jsDocType)
            /* beautify ignore:stop */
            else
              if (this.kickastInstance.options.jsDocHints) jsd.CustomTag("todo", "", "kickast-hint: you may add a JSDoc type annotation to this return expression")


        })
      })
    }
    this.autoJSDocIfAllowed(jsDoc)
  }

  set expression(e) {
    this.body = e
    return e
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      async: this.async,
      body: this.body.node,
      params: this.params.map(p => p.node),
      expression: this._expression,
    }
  }

  toFunction(generate, options) {
    let str = generate({
      type: "ArrowFunctionExpression",
      ...this.node
    }, options)
    return eval(`let f = ${str};f`)
  }
}

export default Lambda;