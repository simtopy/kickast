import util from 'util';
import * as Internal from './internal.js';
import JsdocSequence from './extensions/jsdoc/base/jsdocSequence.js';
import csl from "console"
import repl from "repl"
import * as colors from "colors"

/** @typedef {import('./doc/callbacksAnnotations').Internal.StatementGeneratorCb} Internal.StatementGeneratorCb */
/**
 * A class that implements a JS AST (Abstract Syntaxic Tree) node
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#node-objects} for specifications.
 *
 **/
class Node {
  /**
   * Create a AST Node
   *
   * @param {Node} parent - the parent node in the AST (ex : block)
   *
   **/
  constructor(parent = null) {
    this.parent = parent
    this.kickastInstance = this.parent ? this.parent.kickastInstance : null
    this.comments = []
    this._depth = null
    this._breakpoint = null
  }

  /**
   * info object on the current node and its relations
   **/
  get info() {


    return {
      type: this.constructor.name,
      trueParent: this.trueParent ? this.trueParent.constructor.name : null,
      concreteParent: this.concreteParent ? this.concreteParent.constructor.name : null,
      statement: (this._statement && this._statement !== this) ? this._statement.info : null,
      expression: (this._expression && this._expression !== this) ? this._expression.info : null,
      left: this.left ? this.left.info : null,
      right: this.right ? this.right.info : null,
    }
  }

  get depth() {
    if (!this._depth)
      this._depth = this.trueParent ? this.trueParent.depth + 1 : 0
    return this._depth
  }

  get trueParent() {
    return this.parent
  }

  //doesnt make any difference for simple nodes
  get concreteParent() {
    return this.parent
  }

  set trueParent(p) {
    this.parent = p
  }

  /**
   * the node member is a canonical representation of a AST node, as described by the standard.
   * These informations will be used by the generators to produce the code.
   **/
  get _node() {
    return {
      type: this.constructor.name
    }
  }

  /**
   * Inline debug output for this node
   */
  get debugOutput() {
    let name = null
    let flag = this._flag ? this._flag : null
    if (this instanceof Internal.Program) name = this.constructor.name.red
    else if (this instanceof Internal.Statement) name = this.constructor.name.yellow
    else if (this instanceof Internal.Expression) name = this.constructor.name.green
    else name = this.constructor.name.cyan
    return `${name} ${this._debugOutput ? `[ ${this._debugOutput.gray} ]` : ""} ${flag ? `DebugFlag: ${flag}`.red : ""}`
  }

  get _debugOutput() {
    return undefined
  }

  /**
   * the node member is a canonical representation of a AST node, as described by the standard.
   * These informations will be used by the generators to produce the code.
   **/
  get node() {
    if (this.kickastInstance.options.autoJsDoc && this.comments) {
      if (!this.overridedJSDoc) this.JSDoc()
    }
    if (this.kickastInstance.options.debug) csl.debug(`${`[KICKAST DEBUG]`.white} ${`${"--".repeat(this.depth)}->`.gray} ${this.debugOutput}`)
    if (this.kickastInstance.options.debug && this._breakpoint) {
      csl.debug(`[KICKAST DEBUG] Reached Breakpoint ${this._breakpoint} !\n[KICKAST DEBUG] NODE INFO :\n`, this.info)
      if (this.kickastInstance.options.debug) process.exit(-1)
    }
    return this._node
  }

  /**
   * Add a breakpoint to this node
   * @param {*} breakpoint 
   * @returns 
   */
  breakpoint(breakpoint) {
    this._breakpoint = breakpoint
    return this
  }

  /**
   * Add a debug flag to this node
   * @param {*} flag
   */
  flag(flag) {
    this._flag = flag
    return this
  }

  /**
   * Add a comment to the given node.
   *
   * @param {string | array} com - Inline or Multiline comment
   *
   *
   **/
  comment(com) {
    if (!this.comments) this.concreteParent.comment(com)
    else if (Array.isArray(com)) {
      this.comments.push({
        type: "Block",
        value: com.join("\n")
      })
    } else {
      this.comments.push({
        type: "Line",
        value: com
      })
    }
    return this
  }

  ////////////////////////////////////////////////////////////////////
  // JS DOC
  ///////////////////////////////////////////////////////////////////

  //JS DOC TAGS ANNOTATION

  /**
   * @private
   */
  get jsDocTags() {
    if (!this.comments)
      return this.concreteParent.jsDocTags
    else
      return this._jsDocSequence
  }
  /**
   * @private
   */
  set jsDocTags(sec) {
    if (!this.comments) {
      if (this.concreteParent) {
        this.concreteParent.jsDocTags = sec
      }
      return
    } else if (this._jsDocSequence) {
      this._jsDocSequence = sec.concatenate(this._jsDocSequence)
    } else {
      this._jsDocSequence = sec
    }
  }

  //JS DOC TYPE COMMENT
  get jsDocComment() {
    if (this._commentAnnotation)
      return this._commentAnnotation
    else return ""
  }
  set jsDocComment(com) {
    if (!com) {
      this._commentAnnotation = ""
    }
    this._commentAnnotation = com
  }

  /**
   * @param {import('./extensions/jsdoc/base/jsdocSequence.js').tagBuilderCb} com 
   * @returns 
   */
  JSDoc(com = null, override = false) {
    if (this.overridedJSDoc) return this
    // bubble up to the statement
    if (this.comments === null) {
      this.concreteParent.JSDoc(com, override)
      return this
    }
    let custom = new JsdocSequence(com)
    let res = null
    if (override) {
      res = custom
      this.overridedJSDoc = true
    } else {
      res = custom.concatenate(this._jsDocSequence)
      //Empty
    }
    if (res.length > 0) {
      this._jsDocSequence = null
      const toPrint = res.toStringArray()
      return this.comment(toPrint)
    }
  }

  /**
   * Apply a method to a given object if it exists, and return it
   *
   * Usually, this will be use internaly to apply modifications on expression or statements,
   * for instance, defining the result action of a IF statement if the test succeed, 
   * In this case being applied to a newly created « consequent » statement or block.
   *
   * Behaviour is defined as : <br/>
   * - will return object without modifications if method undefined or null<br/>
   * - will return method if method is not a instance of function<br/>
   * - will return the modified object otherwise <br/>
   * 
   * Apply improves usability by allowing users to skip return statement when they write kickast callbacks
   *
   * @param {Function} method - The function to apply on the object
   * @param {object} object - The object the method will be applied on
   * @param {} args -  Others arguments of the method
   *
   * @returns {object} Returns the modified object if method is a valid instance of Function
   **/
  apply(method, object, ...args) {
    if (!method) return object
    if (typeof method !== "function") return method
    else {
      method(object, ...args)
      return object
    }
  }

  /**
   * Returns v if v is an array. Otherwise, returns [v]
   **/
  array(v) {
    if (Array.isArray(v)) return v
    else return [v]
  }

  /**
   * log a JS representation for the current node and its parent
   *
   * @param {Number} depth - The recursion depth of the exploration 
   **/
  log(depth = Infinity) {
    console.log(util.inspect(this.node, false, depth, true))
  }

  /**
   * @private
   * @param {*} depthp 
   */
  getParents(depthp) {
    let depth = depthp
    const res = []
    let current = this
    while (depth > 0) {
      res.push(current.parent)
      current = this.concreteParent
      depth--
    }
    return res
  }

  /**
   * @private
   * @param {*} jsDoc 
   */
  autoJSDocIfAllowed(jsDoc) {
    let doc = false
    if (this.concreteParent instanceof Internal.Declaration) {
      doc = true
    } else if (this.concreteParent instanceof Internal.Property) {
      doc = true
    }

    if (this.concreteParent instanceof Internal.ForStatement) {
      doc = false
    } else if (this.concreteParent instanceof Internal.ForInStatement) {
      doc = false
    } else if (this.concreteParent instanceof Internal.ForOfStatement) {
      doc = false
    }
    if (doc) this.jsDocTags = new Internal.JsdocSequence(jsDoc)
  }
}

export default Node;