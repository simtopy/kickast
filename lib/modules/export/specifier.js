import * as Internal from '../../internal.js';

export default class ExportSpecifier extends Internal.ModuleSpecifier {

  constructor(node, exported, local) {
    super(node);
    this.exported = new Internal.Identifier(this, exported);
    this.local = new Internal.Identifier(this, local || exported)
  }

  get _node() {
    return {
      type: this.constructor.name,
      exported: this.exported.node,
      local: this.local.node
    }
  }

  /*
  toString() {
    if(!this.exported) throw Error("no exported");
    return this.exported;
  }
  */

};
