import * as Internal from '../../internal.js';

export default class ExportAllDeclaration extends Internal.ModuleDeclaration {

  constructor(node, source, exported) {
    super(node);
    this._source = source ? new Internal.Literal(this, source) : null;
    this._exported = exported ? new Internal.Identifier(this, exported) : null
  }

  get _node() {
    return {
      type: this.constructor.name,
      source: this._source ? this._source.node : null,
      exported: this._exported ? this._exported.node : null
    }
  }

  /**
   *
   *
   * @param {string} exported
   * @returns
   */
  As(exported) {
    this._exported = new Internal.Identifier(this, exported)
    return this
  }
};
