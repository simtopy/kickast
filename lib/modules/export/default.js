import * as Internal from '../../internal.js';

export default class ExportDefaultDeclaration extends Internal.ModuleDeclaration {

  constructor(node, declaration) {
    super(node);
    this._declaration = this.apply(declaration, new Internal.PlaceholderExpression(this));
  }

  get _node() {
    return {
      type: this.constructor.name,
      declaration: this._declaration.node
    }
  }

  /*
  toString() {
    if(!this.declaration) throw Error("no declaration");
    return `export default ${this.declaration.toString()};`;
  }
  */

};
