import * as Internal from '../../internal.js';

export default class ExportNamedDeclaration extends Internal.ModuleDeclaration {

  constructor(node, declaration, specifiers = [], source) {
    super(node);
    this._source = source ? new Internal.Literal(this, source) : null;
    this._declaration = declaration ? this.apply(declaration, new Internal.PlaceholderStatement(this)) : null;
    this.specifiers = specifiers;
  }

  get _node() {
    return {
      type: this.constructor.name,
      declaration: this._declaration ? this._declaration.node : null,
      specifiers: this.specifiers.map(s => s.node),
      source: this._source ? this._source.node : null
    }
  }

  /**
   *
   *
   * @param {string[]} locals
   * @returns
   */
  Local(...locals) {
    locals.forEach(e => {
      if (Array.isArray(e)) {
        this.specifiers.push(new Internal.ExportSpecifier(this, e[1], e[0]));
      } else {
        this.specifiers.push(new Internal.ExportSpecifier(this, e));
      }
    });
    return this;
  }

};