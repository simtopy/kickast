import * as Internal from '../../internal.js';

export default class ImportSpecifier extends Internal.ModuleSpecifier {

  constructor(node, imported, local) {
    super(node)
    this.imported = new Internal.Identifier(this, imported)
    this.local = new Internal.Identifier(this, local || imported)
  }

  get _node() {
    return {
      type: this.constructor.name,
      imported: this.imported.node,
      local: this.local.node
    }
  }
};
