import * as Internal from '../../internal.js';

export default class ImportDeclaration extends Internal.ModuleDeclaration {

  constructor(node, source) {
    super(node);
    this.BindAliases();
    if(source) {
      this._source = new Internal.Literal(this, source);
    }
    this.specifiers = [];
  }

  get _node() {
    return {
      type: this.constructor.name,
      specifiers: this.specifiers.map(s => s.node),
      source: this._source.node
    }
  }

  /**
   *
   *
   * @param {string[]} locals - Local identifiers
   * @returns
   */
  Local(...locals) {
    locals.forEach(e => {
      if (Array.isArray(e)) {
        this.specifiers.push(new Internal.ImportSpecifier(this, e[0], e[1]));
      } else {
        this.specifiers.push(new Internal.ImportSpecifier(this, e));
      }
    });
    return this;
  }

  /**
   *
   *
   * @param {string} local - Local identifier
   * @returns
   */
  Default(local) {
    this.specifiers.push(new Internal.ImportDefaultSpecifier(this, local));
    return this;
  }
};
