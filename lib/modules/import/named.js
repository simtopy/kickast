import * as Internal from '../../internal.js';

export default class ImportNamedSpecifier extends Internal.ModuleSpecifier {

  constructor(node, local) {
    super(node);
    this.local = local ? new Internal.Identifier(local) : null
  }

  get _node() {
    return {
      type: this.constructor.name,
      local: this.local.node
    }
  }

  /**
   *
   *
   * @param {string} local - local identifier
   * @returns
   */
  As(local) {
    this.local = new Internal.Identifier(local)
    return this;
  }
};
