import * as Internal from '../../internal.js';


export default class ImportDefaultSpecifier extends Internal.ModuleSpecifier {

  constructor(node, local) {
    super(node)
    this.local = new Internal.Identifier(this, local)
  }

  get _node() {
    return {
      type: this.constructor.name,
      local: this.local.node
    }
  }
};
