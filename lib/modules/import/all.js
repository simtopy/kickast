import * as Internal from '../../internal.js';

export default class ImportAllDeclaration extends Internal.ModuleDeclaration {

  constructor(node) {
    super(node);
    this.BindAliases();
    this.source = null;
    this.specifiers = [];
  }

  get _node() {
    return {
      type: this.constructor.name,
      specifiers: this.specifiers.map(s => s.node),
      source: this._source.node
    }
  }

  /**
   *
   *
   * @param {string} local - Local identifier
   * @returns
   */
  As(local) {
    this.specifiers.push(new Internal.ImportNamedSpecifier(this, local));
    return this;
  }
};
