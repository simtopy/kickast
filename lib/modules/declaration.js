import * as Internal from '../internal.js';

export default class ModuleDeclaration extends Internal.Node {

  constructor(node) {
    super(node);
    this.BindAliases()
    this._declaration = null;
    this._source = null;
  }

  get _node() {
    const supernode = super._node
    if (this._declaration) return this._declaration.node
    else return {
      ...supernode,
      comments: this.comments
    }
  }

  set declaration(s) {
    this._declaration = s;
    return s;
  }

  /**
   *
   *
   * @param {string} path
   * @returns
   */
  From(path) {
    this._source = new Internal.Literal(this, path);
    return this;
  }

  BindAliases(chainable = null, unchainable = []) {
    //this.bindings(Internal.ModuleApi, chainable, unchainable);
  }
};