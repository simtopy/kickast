import * as Internal from '../internal.js';

export default class ModuleSpecifier extends Internal.Node {

  /**
   *Creates an instance of ModuleSpecifier.
   * @param {} node
   * @param {string} [local=""]
   */
  constructor(node, local = "") {
    super(node);
    this.local = local;
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      local: this.local.node
    }
  }

};