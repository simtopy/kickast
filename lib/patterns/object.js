import * as Internal from '../internal.js';

export default class ObjectPattern extends Internal.Pattern {

  constructor(node, callback) {
    super(node)
    this.properties = []
    if (callback) callback(this)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      properties: this.properties.map(p => p.node),
    }
  }

  /**
   *
   *
   * @param {string} name
   * @param {string | Internal.ExpressionGeneratorCb} init
   */
  Property(name, init) {
    if (init !== undefined) {
      this.properties.push(new Internal.AssignmentPattern(this, name, init))
    } else {
      let key = new Internal.Identifier(this, name)
      this.properties.push(new Internal.Property(this, key, null, "init", false, false, true))
    }
  }

  /**
   *
   *
   * @param {string} name
   */
  Rest(name) {
    this.properties.push(new Internal.RestElement(this, name))
  }
};