import * as Internal from '../internal.js';

export default class ArrayPattern extends Internal.Pattern {

  constructor(node, callback) {
    super(node)
    this.elements = []
    if (callback) callback(this)
  }

  get _node() {
    return {
      type: this.constructor.name,
      elements: this.elements.map(p => p ? p.node : null),
    }
  }

  /**
   *
   *
   * @param {string} name
   * @param {string} init
   * @returns
   */
  Identifier(name, init) {
    if (init !== undefined) {
      // Assignment      
      this.elements.push(new Internal.AssignmentPattern(this, name, init))
    } else {
      // Identifier
      this.elements.push(new Internal.Identifier(this, name))
    }
    return this
  }

  /**
   *
   *
   * @param {string} name
   * @returns
   */
  Rest(name) {
    this.elements.push(new Internal.RestElement(this, name))
    return this
  }
};