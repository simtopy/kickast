import * as Internal from '../internal.js';

export default class RestElement extends Internal.Pattern {

  constructor(node, name) {
    super(node)
    this.argument = new Internal.Identifier(this, name)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      argument: this.argument.node,
    }
  }
};