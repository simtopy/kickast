import * as Internal from '../internal.js';

export default class AssignmentPattern extends Internal.Pattern {

  /**
   *Creates an instance of AssignmentPattern.
   * @param {Internal.Node} node
   * @param {Internal.ExpressionGeneratorCb | string} left
   * @param {Internal.ExpressionGeneratorCb | string} right
   */
  constructor(node, left, right) {
    super(node)
    this.left = (typeof left === "function") ? this.apply(left, new Internal.PlaceholderExpression(this)) : new Internal.Identifier(this, left)
    this.right = (typeof right === "function") ? this.apply(right, new Internal.PlaceholderExpression(this)) : new Internal.Literal(this, right)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      left: this.left.node,
      right: this.right.node
    }
  }
};