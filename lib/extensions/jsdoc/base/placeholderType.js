import typeContainer from "./typeContainer.js"

export default class PlaceholderType extends typeContainer {
  constructor() {
    super()
    this.content = null
  }
}