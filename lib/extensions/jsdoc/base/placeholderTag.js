import TagContainer from "./tagContainer.js"

export default class PlaceholderTag extends TagContainer {
  constructor() {
    super()
    this.content = null
  }
}