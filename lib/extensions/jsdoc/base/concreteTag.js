import AbstractTag from './abstractTag.js'
import PlaceholderType from './placeholderType.js'

/**
 * @typedef {import('./placeholderType.js').default} PlaceholderType
 * @typedef {function(PlaceholderType) : void} typeGeneratorCb
 */

export default class ConcreteTag extends AbstractTag {
  /**
   * @param {String} tag 
   * @param {String|typeGeneratorCb|null} type 
   * @param {String|null} variable 
   * @param {String|null} comment 
   */
  constructor(tag = null, type = null, variable = null, comment = null) {
    super()
    this.tag = tag
    if (typeof(type) === "function") {
      this.type = new PlaceholderType()
      type(this.type)
      this.type = this.type.toString()
    } else {
      this.type = type
    }
    this.variable = variable
    this.comment = comment
  }

  toString() {
    if (!this.tag) {
      throw Error("Cannot expand Tag : tag has not been defined")
    }
    const maybeType = this.type ? ` {${this.type}}` : ""
    const maybeVariable = this.variable ? ` ${this.variable}` : ""
    const maybeComment = this.comment ? ` - ${this.comment}` : ""
    return `@${this.tag}${maybeType}${maybeVariable}${maybeComment}`
  }
}