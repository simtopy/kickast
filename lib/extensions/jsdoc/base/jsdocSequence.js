/* eslint-disable max-classes-per-file */
import TagContainer from "./tagContainer.js"
import IncludedTag from "../tags/include.js"

/**
 * @typedef {function(sequenceBuilderApi): void} tagBuilderCb
 */

class sequenceBuilderApi extends TagContainer {
  constructor() {
    super()
    this.tagArray = []
  }

  Include() {
    this.content = new IncludedTag
  }

  set content(tag) {
    this.tagArray.push(tag)
  }
}

export default class JsdocSequence {
  /**
   * 
   * @param {tagBuilderCb?} tagBuilderCb 
   */
  constructor(tagBuilderCb, toAppend = null) {
    if (tagBuilderCb != null) {
      const seq = new sequenceBuilderApi()
      tagBuilderCb(seq)
      this.tagArray = seq.tagArray
    } else {
      this.tagArray = []
    }
    if (toAppend != null) {
      this.tagArray = [...this.tagArray, ...toAppend]
    }
  }

  get tags() {
    return [...this.tagArray]
  }

  get tag() {
    return this.tagArray[this.length - 1]
  }

  get length() {
    return this.tagArray.length
  }

  toStringArray() {
    this.tagArray.unshift("")
    this.tagArray.push("")
    if (this.length !== 0)
      return this.tagArray.map(t => t.toString()).map(s => `* ${s}`)
    else return []
  }

  /**
   * @param {JsdocSequence} toAppend
   */
  concatenate(toAppend) {
    if (toAppend != null)
      return new JsdocSequence(null, [...this.tagArray, ...toAppend.tags])
    else return new JsdocSequence(null, [...this.tagArray])
  }
}