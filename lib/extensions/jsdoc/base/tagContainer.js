import AbstractTag from './abstractTag.js'
import AuthorTag from "../tags/author.js"
import TypedefTag from "../tags/typedef.js"
import TypeTag from "../tags/type.js"
import ParamTag from "../tags/param.js"
import DescTag from "../tags/description.js"
import AsyncTag from "../tags/async.js"
import SeeTag from "../tags/see.js"
import ReturnsTag from "../tags/returns.js"
import ConcreteTag from "./concreteTag.js"
import PropertyTag from "../tags/property.js"
import ClassTag from "../tags/class.js"
import GetterTag from '../tags/getter.js'
import SetterTag from '../tags/setter.js'
import StaticTag from '../tags/static.js'
import MethodTag from '../tags/method.js'
import ConstructorTag from '../tags/constructor.js'
import ExtendsTag from '../tags/extends.js'
import Raw from './raw.js'
import FiresTag from '../tags/fires.js'
import ListenTag from '../tags/listen.js'
import EventTag from '../tags/event.js'
import YieldsTag from '../tags/yields.js'
import { PrivateTag, ProtectedTag, PublicTag } from '../tags/access.js'

export default class TagContainer extends AbstractTag {
  constructor() {
    super()
  }

  toString() {
    if (!this.content) {
      throw Error("I am a placebolder you dumbass")
    }
    return this.content.toString()
  }

  //API

  /**
   * Specify the author of a code part
   * @param {string} authorName 
   * @param {string} authorMail 
   * @param {string} comm 
   */
  Author(authorName, authorMail, comm) {
    this.content = new AuthorTag(authorName, authorMail, comm)
  }

  /**
   * Specify that this function is async
   */
  Async() {
    this.content = new AsyncTag()
  }

  Class() {
    this.content = new ClassTag()
  }

  /**
   * Describe an identifier
   * 
   * @param {string} content
   */
  Description(content) {
    this.content = new DescTag(content)
  }

  /**
   * Class inheritance
   * 
   * @param {string} superclass 
   */
  Extends(superclass) {
    return this.content = new ExtendsTag(superclass)
  }

  /**
   * Document an event
   * @param {string} namespace - Usually class, object or module name
   * @param {string} eventName 
   * @returns 
   */
  Event(namespace, eventName) {
    return this.content = new EventTag(namespace, eventName)
  }

  /**
   * Indicate that a method fires an event
   * @param {string} namespace - Usually class, object or module name
   * @param {string} eventName 
   * @returns 
   */
  Fires(namespace, eventName) {
    return this.content = new FiresTag(namespace, eventName)
  }

  Getter() {
    this.content = new GetterTag()
  }

  /**
   * Indicate that a given context listen to an event
   * @param {string} namespace - Usually class, object or module name
   * @param {string} eventName 
   * @returns 
   */
  Listen(namespace, eventName) {
    return this.content = new ListenTag(namespace, eventName)
  }

  Method() {
    this.content = new MethodTag()
  }

  Constructor() {
    this.content = new ConstructorTag()
  }

  Setter() {
    this.content = new SetterTag()
  }

  Static() {
    this.content = new StaticTag()
  }

  /**
   * Define or aliase a type
   * 
   * @param {import('./concreteTag.js').typeGeneratorCb} type - Type definition through callback or simple string
   * @param {string} typeName - Name of the type
   * @param {string} comm - Facultative additionnal commment
   */
  Typedef(type, typeName, comm) {
    this.content = new TypedefTag(type, typeName, comm)
  }

  /**
   * Type a variable
   * 
   * @param {import('./concreteTag.js').typeGeneratorCb} type - Type definition through callback or simple string
   * @param {string} variable - Identifier of the variable to document
   * @param {string} com - additionnal comment
   * 
   */
  Type(type, variable, com) {
    this.content = new TypeTag(type, variable, com)
  }

  /**
   * Document a function parameter
   * 
   * @param {import('./concreteTag.js').typeGeneratorCb} paramType - Type definition through callback or simple string
   * @param {string} paramName - Name of param or param' property
   * @param {string} comm- Additionnal comment
   * 
   */
  Param(paramType, paramName, comm) {
    this.content = new ParamTag(paramType, paramName, comm)
  }

  /**
   * Import a type in the current scope
   * @param {string} alias - Type alias in the local scope
   * @param {string} modulePath - Relative path to the module
   * @param {string} toImport - Object or type to fetch
   * @param {string} comm - Additionnal comment
   */
  ImportType(alias, modulePath, toImport, comm) {
    this.content = new TypedefTag(t => t.Import(modulePath, toImport), alias, comm)
  }

  /**
   * Custom Tag
   * @param {string} tag 
   * @param {import('./concreteTag.js').typeGeneratorCb} typeDefinition - Type definition through callback or simple string
   * @param {string} variable - Variable or target it applies to
   * @param {string} com - Additionnal commentary
   */
  CustomTag(tag, typeDefinition, variable, com) {
    this.content = new ConcreteTag(tag, typeDefinition, variable, com)
  }

  /**
   * Refers to another type documentation
   * @param {import('./concreteTag.js').typeGeneratorCb} typeDefinition - Type definition through callback or simple string
   */
  See(typeDefinition) {
    this.content = new SeeTag(typeDefinition)
  }

  /**
   * Refers to a web ressource
   * @param {string} uri
   */
  SeeLink(uri) {
    this.content = new SeeTag(t => t.Link(uri))
  }

  Property(propType, propName, com) {
    this.content = new PropertyTag(propType, propName, com)
  }

  Public() {
    this.content = new PublicTag()
  }

  Private() {
    this.content = new PrivateTag()
  }

  Protected() {
    this.content = new ProtectedTag()
  }

  /**
   * Documents a function return type 
   * 
   * @param {import('./concreteTag.js').typeGeneratorCb} typeDefinition - Type definition through callback or simple string
   * @param {string} com - Additionnal commentary
   */
  Returns(typeDefinition, com) {
    this.content = new ReturnsTag(typeDefinition, com)
  }

  Raw(com) {
    this.content = new Raw(com)
  }

  /**
   * Specify type yielded by a generator function
   * @param {import('./concreteTag.js').typeGeneratorCb} type 
   * @param {string} com 
   */
  Yields(type, com) {
    this.content = new YieldsTag(type, com)
  }
}