import AbstractType from "./abstractType.js"

export default class ConcreteJSType extends AbstractType {
  constructor(definition) {
    super()
    this.definition = definition
  }

  toString() {
    return this.definition
  }

  get api() {
    return new ConcreteTypeApi(this)
  }
}