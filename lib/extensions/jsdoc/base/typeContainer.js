import AbstractType from "./abstractType.js"
import { CustomType } from "../types/custom.js"
import { JSTypedFunction } from "../types/function.js"
import { JsDocImportedType } from "../types/import.js"
import { LinkType } from "../types/link.js"
import * as simple from "../types/simples.js"
import UnionType from "../types/union.js"
import MaybeType from "../types/maybe.js"
import ArrayType from "../types/array.js"
import JustType from "../types/just.js"
import OptionalType from "../types/optional.js"

export default class TypeContainer extends AbstractType {
  constructor() {
    super()
  }

  toString() {
    if (!this.content) {
      throw Error("I am a placebolder you dumbass")
    }
    return this.content.toString()
  }

  //Type generator api

  get Any() {
    this.content = new simple.JSAny()
  }

  get Boolean() {
    this.content = new simple.JSBoolean()
  }

  get String() {
    this.content = new simple.JSString()
  }

  get Number() {
    this.content = new simple.JSNumber()
  }

  get Object() {
    this.content = new simple.JSObject()
  }

  get Null() {
    this.content = new simple.JSNull()
  }

  get Void() {
    this.content = new simple.JSVoid()
  }

  get Function() {
    this.content = new simple.JSFunction()
  }

  /**
   * Declare a typed function
   * 
   * @param {import('../types/function').JsDocParamTypingCb|[]|string[]} params - Params definition
   * @param {import('./concreteTag').typeGeneratorCb|string} returnType 
   */
  TypedFunction(params, returnType) {
    this.content = new JSTypedFunction(params, returnType)
  }

  /**
   * 
   * @param {string} modulePath 
   * @param {string} toImport 
   */
  Import(modulePath, toImport) {
    this.content = new JsDocImportedType(modulePath, toImport)
  }

  /**
   * 
   * @param {string} name 
   */
  UserDefinedType(name) {
    this.content = new CustomType(name)
  }

  /**
   * 
   * @param {string} link - an URI to refers to
   */
  Link(link) {
    this.content = new LinkType(link)
  }

  /**
   * 
   * @param {import('../types/union.js').unionGeneratorCb} unionGeneratorCb 
   */
  Union(unionGeneratorCb) {
    this.content = new UnionType(unionGeneratorCb)
  }

  /**
   * @getter
   * @return {TypeContainer}
   */
  get Maybe() {
    this.content = new MaybeType()
    return this.content.container
  }

  /**
   * Specify that a parameter is optional
   * @getter
   * @return {TypeContainer}
   */
  get Optional() {
    this.content = new OptionalType()
    return this.content.container
  }

  /**
   * @getter
   * @return {TypeContainer}
   */
  get Array() {
    this.content = new ArrayType()
    return this.content.container
  }

  /**
   * @getter
   * @return {TypeContainer}
   */
  get Just() {
    this.content = new JustType()
    return this.content.container
  }

  // ALIASES

  /**
   * @getter
   * @return {TypeContainer}
   */
  get Nullable() {
    this.content = new MaybeType()
    return this.content.container
  }

  /**
   * @getter
   * @return {TypeContainer}
   */
  get NotNullable() {
    this.content = new JustType()
    return this.content.container
  }
}