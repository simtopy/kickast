import AbstractTag from './abstractTag.js'
import PlaceholderType from './placeholderType.js'

/**
 * @typedef {import('./placeholderType.js').default} PlaceholderType
 * @typedef {function(PlaceholderType) : void} typeGeneratorCb
 */

export default class Raw extends AbstractTag {
  /**
   * @param {String|null} content
   */
  constructor(content = null) {
    super()
    this.content = content
  }

  toString() {
    if (!this.content) {
      throw Error("No content in Raw")
    }
    return `${this.content}`
  }
}