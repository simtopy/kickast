import ConcreteTag from "../base/concreteTag.js";

/**
 * Type a variable
 */
export default class ExtendsTag extends ConcreteTag {
  /**
   * 
   * @param {string} superclass
   */
  constructor(superclass) {
    super("superclass", superclass)
  }
}