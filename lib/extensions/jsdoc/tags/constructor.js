import ConcreteTag from "../base/concreteTag.js"
export default class ConstructorTag extends ConcreteTag {
  /**
   * 
   */
  constructor() {
    super("constructor", "", "", "")
  }
}