import ConcreteTag from "../base/concreteTag.js"
export default class SeeTag extends ConcreteTag {
  constructor(type) {
    super("see", type, "", "")
  }
}