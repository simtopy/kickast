import ConcreteTag from "../base/concreteTag.js"
import TagContainer from "../base/tagContainer.js"
export default class IncludedTag extends TagContainer {
  constructor(tag) {
    super()
    this.content = tag
  }
}