import ConcreteTag from "../base/concreteTag.js";

/**
 * Define a new type
 */
export default class TypedefTag extends ConcreteTag {
  /**
   * 
   * @param {import('../concreteTag.js'.typeGeneratorCb|string} typeDefinition
   * @param {string} typeName 
   * @param {string} comment 
   */
  constructor(typeDefinition, typeName, comment) {
    super("typedef", typeDefinition, typeName, comment)
  }
}