import ConcreteTag from "../base/concreteTag.js"
export class PrivateTag extends ConcreteTag {
  /**
   * 
   */
  constructor() {
    super("private", "", "", "")
  }
}

export class PublicTag extends ConcreteTag {
  /**
   * 
   */
  constructor() {
    super("public", "", "", "")
  }
}

export class ProtectedTag extends ConcreteTag {
  /**
   * 
   */
  constructor() {
    super("protected", "", "", "")
  }
}