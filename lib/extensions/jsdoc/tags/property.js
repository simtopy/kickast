import ConcreteTag from "../base/concreteTag.js";

/**
 * Type a variable
 */
export default class PropertyTag extends ConcreteTag {
  /**
   * 
   * @param {import('../concreteTag.js'.typeGeneratorCb|string} propertyType
   * @param {string} propertyName
   * @param {string} comment 
   **/
  constructor(propertyType, propertyName, comment) {
    super("property", propertyType, propertyName, comment)
  }
}