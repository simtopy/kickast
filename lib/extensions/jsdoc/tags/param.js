import ConcreteTag from "../base/concreteTag.js";

/**
 * Type a variable
 */
export default class ParamTag extends ConcreteTag {
  /**
   * 
   * @param {import('../concreteTag.js'.typeGeneratorCb|string} paramType
   * @param {string} paramName
   * @param {string} comment 
   **/
  constructor(paramType, paramName, comment) {
    super("param", paramType, paramName, comment)
  }
}