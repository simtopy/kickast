import ConcreteTag from "../base/concreteTag.js"
export default class StaticTag extends ConcreteTag {
  /**
   * 
   */
  constructor() {
    super("static", "", "", "")
  }
}