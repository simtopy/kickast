import ConcreteTag from "../base/concreteTag.js"
export default class AuthorTag extends ConcreteTag {
  /**
   * 
   * @param {string|import("../concreteTag.js".typeGeneratorCb)} type 
   * @param {string} authorName
   * @param {string?} authorMail
   * @param {string?} comment 
   */
  constructor(authorName, authorMail = null, comment) {
    super("author", null, `${authorName + !authorMail ? "" :`<${authorMail}>`}`, comment)
  }
}