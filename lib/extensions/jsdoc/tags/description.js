import ConcreteTag from "../base/concreteTag.js";

/**
 * Type a variable
 */
export default class DescTag extends ConcreteTag {

  /**
   * 
   * @param {string} desc
   */
  constructor(desc) {
    super("description", "", desc, "")
  }
}