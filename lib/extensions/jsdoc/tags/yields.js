import ConcreteTag from "../base/concreteTag.js";

/**
 * Type a variable
 */
export default class YieldsTag extends ConcreteTag {
  /**
   * 
   * @param {import('../concreteTag.js'.typeGeneratorCb|string} returnType
   * @param {string} comment 
   **/
  constructor(returnType, comment) {
    super("yields", returnType, "", comment)
  }
}