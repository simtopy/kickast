import ConcreteTag from "../base/concreteTag.js";

/**
 * Specify that a given method fires an event
 */
export default class FiresTag extends ConcreteTag {
  /**
   * 
   * @param {string} namespace
   * @param {string} eventName
   */
  constructor(namespace, eventName) {
    super("fires", [namespace, eventName].join('#'))
  }
}