import ConcreteTag from "../base/concreteTag.js";

/**
 * Document an event
 */
export default class EventTag extends ConcreteTag {
  /**
   * 
   * @param {string} namespace
   * @param {string} eventName
   */
  constructor(namespace, eventName) {
    super("event", [namespace, eventName].join('#'))
  }
}