import ConcreteTag from "../base/concreteTag.js";

/**
 * Type a variable
 */
export default class TypeTag extends ConcreteTag {
  /**
   * 
   * @param {string|import("../concreteTag.js".typeGeneratorCb)} type 
   * @param {string} variable
   * @param {string} comment 
   */
  constructor(type, variable, comment) {
    super("type", type, variable, comment)
  }
}