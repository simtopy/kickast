import ConcreteTag from "../base/concreteTag.js";

/**
 * Define a new type
 */
export default class ListenTag extends ConcreteTag {
  /**
   * 
   * @param {string} namespace
   * @param {string} eventName
   */
  constructor(namespace, eventName) {
    super("listens", [namespace, eventName, "event"].join('#'))
  }
}