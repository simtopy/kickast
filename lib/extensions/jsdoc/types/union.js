// eslint-disable-next-line max-classes-per-file
import TypeContainer from "../base/typeContainer.js"
import ConcreteJSType from "../base/concreteType.js"

/**
 * @typedef {import('../base/concreteTag').typeGeneratorCb} typeGeneratorCb
 * @typedef {typeGeneratorCb} unionGeneratorCb
 */

export default class UnionType extends ConcreteJSType {
  /**
   * 
   * @param {unionGeneratorCb} unionGeneratorCb 
   */
  constructor(unionGeneratorCb) {
    //If we declare this class outside we have circual dependencies problems
    const generator = new class extends TypeContainer {
      constructor() {
        super()
        this.types = []
      }
      set content(type) {
        this.types.push(type)
      }
    }
    unionGeneratorCb(generator)
    let osep = ''
    let csep = ''
    if (generator.types.length > 1) {
      osep = '('
      csep = ')'
    }

    super(osep + generator.types.join('|') + csep)
  }
}