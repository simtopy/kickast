import JSType from "../base/concreteType.js"
export class JsDocImportedType extends JSType {
  /**
   * 
   * @param {string} modulePath
   * @param {string} toImport - Type to get from module
   */
  constructor(modulePath, toImport) {
    super(`import('${modulePath}').${toImport}`)
  }
}