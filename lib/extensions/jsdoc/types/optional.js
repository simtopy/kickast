import TypeContainer from "../base/typeContainer.js"
import ConcreteJSType from "../base/concreteType.js"

/**
 * @typedef {import('../base/concreteTag').typeGeneratorCb} typeGeneratorCb
 */

export default class OptionalType extends ConcreteJSType {
  constructor() {
    super()
    this.container = new TypeContainer()
  }

  toString() {
    return this.container.toString() + "="
  }

}