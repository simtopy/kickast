import ConcreteJSType from "../base/concreteType.js"
//Prefix them with JS to avoid conflict with built-in class

export class JSAny extends ConcreteJSType {
  constructor() {
    super("*")
  }
}
export class JSBoolean extends ConcreteJSType {
  constructor() {
    super("boolean")
  }
}

export class JSString extends ConcreteJSType {
  constructor() {
    super("string")
  }
}

export class JSNumber extends ConcreteJSType {
  constructor() {
    super("number")
  }
}

export class JSObject extends ConcreteJSType {
  constructor() {
    super("object")
  }
}

export class JSFunction extends ConcreteJSType {
  constructor() {
    super("function")
  }
}

export class JSNull extends ConcreteJSType {
  constructor() {
    super("null")
  }
}

//For function return only
export class JSVoid extends ConcreteJSType {
  constructor() {
    super("void")
  }
}