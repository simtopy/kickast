import ConcreteJSType from "../base/concreteType.js"
export class RawType extends ConcreteJSType {
  constructor(definition) {
    super(definition)
  }
}