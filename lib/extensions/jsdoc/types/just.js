import TypeContainer from "../base/typeContainer.js"
import PlaceholderType from "../base/placeholderType.js"
import ConcreteJSType from "../base/concreteType.js"

/**
 * @typedef {import('../base/concreteTag').typeGeneratorCb} typeGeneratorCb
 */

export default class JustType extends ConcreteJSType {
  constructor() {
    super()
    this.container = new TypeContainer()
  }

  toString() {
    return this.container.toString() + "!"
  }

}