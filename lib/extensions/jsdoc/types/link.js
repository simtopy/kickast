import ConcreteJSType from "../base/concreteType.js"

export class LinkType extends ConcreteJSType {
  /**
   * 
   * @param {string} uri 
   */
  constructor(uri) {
    super(`@link ${uri}`)
  }
}