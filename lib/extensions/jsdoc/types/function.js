import PlaceholderType from "../base/placeholderType.js";
import { JSFunction } from "../types/simples.js";
import { RawType } from "./raw.js";

class ParamGenerator {
  constructor() {
    this.params = []
  }
  /**
   * 
   * @param {import("../base/concreteTag.js").typeGeneratorCb} typeGeneratorCb 
   */
  Param(typeGeneratorCb) {
    const res = new PlaceholderType()
    typeGeneratorCb(res)
    this.params.push(res)
  }
}

/**
 * @typedef {function(ParamGenerator): void} JsDocParamTypingCb
 */

export class JSTypedFunction extends JSFunction {

  /**
   * 
   * @param {[]|String[]|JsDocParamTypingCb} params 
   * @param {string|import("../base/concreteTag.js").typeGeneratorCb} returnType
   */
  constructor(params, returnType) {
    super("function")
    if (params instanceof Function) {
      const paramGenerator = new ParamGenerator()
      params(paramGenerator)
      this.params = paramGenerator.params
    } else if (params instanceof Array) {
      this.params = params.map(p => new RawType(p))
    } else if (typeof (params) === "string") {
      this.params = [new RawType(params)]
    } else {
      throw Error("Unhandled Params parameter")
    }

    //Return Type
    if (typeof (returnType) === "string")
      this.returnType = new RawType(returnType)
    else if (returnType instanceof Function) {
      this.returnType = new PlaceholderType()
      returnType(this.returnType)
      this.returnType
    }
  }

  toString() {
    return `function(${this.params.map(p => p.toString()).join(',')}) : ${this.returnType.toString()}`
  }
}