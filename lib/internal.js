/*
  Library modules loading strategy
*/

// Types

/**
 * @namespace callbacksAnnotations
 **/

/**
 * @typedef {function(StatementContainer) : void  } StatementGeneratorCb
 * @memberof callbacksAnnotations
 **/

/**
 * @typedef {function(SwitchStatement) : void  } SwitchGeneratorCb
 * @memberof callbacksAnnotations
 **/

/**
 * @typedef { function(ExpressionContainer) : void} ExpressionGeneratorCb
 * @memberof callbacksAnnotations
 **/

/**
 * @typedef {function(ObjectExpression) : void} ObjectGeneratorCb
 * @memberof callbackAnnotations
 **/

/**
 * @typedef {function(ArrayExpression) : void} ArrayGeneratorCb
 * @memberof callbacksAnnotations
 **/

/**
 * @typedef {function(ObjectPattern) : ObjectExpression} ObjectPatternCb
 * @memberof callbacksAnnotations
 */

/**
 * @typedef {function(ArrayPattern) : ArrayExpression} ArrayPatternCb
 * @memberof callbacksAnnotations
 **/

/**
 * @typedef {function(TemplateLiteral) : void} TemplateLiteralGeneratorCb
 * @memberof callbackAnnotations
 **/

/**
 * @typedef {import('./lambda.js').ParameterApi} ParameterApi
 * @typedef {function(ParameterApi) : void} ParamsGeneratorCb
 * @memberof callbacksAnnotations
 */

/**
 * @typedef {function(ClassDeclaration) : void} ClassGeneratorCb
 * @memberof callbacksAnnotations
 */

/**
 * @typedef {import('./extensions/jsdoc/base/concreteTag.js').typeGeneratorCb} typeGeneratorCb
 */

/**
 * JS Primitives
 * @typedef {(string|number|boolean|undefined|null|BigInt|symbol)} primitives
 **/

/**
 * Parameters generator for function, methods, etc...
 * @typedef {([] | string[] | ParamsGeneratorCb)} paramsGenerator
 **/

// Utils
import acorn from 'acorn';
const parse = acorn.parse;

// interfaces
/** @typedef {from './node.js')} Node **/
import Node from './node.js'

/** @typedef {from './node.js')} VirtualNode **/
import VirtualNode from './virtualNode.js';

/** @typedef {from './pattern.js')} Pattern **/
import Pattern from './pattern.js'

/** @typedef {from './property.js')} Property **/
import Property from './property.js'
// patterns
/** @typedef {from './patterns/rest.js')} RestElement **/
import RestElement from './patterns/rest.js'

/** @typedef {from './patterns/array.js')} ArrayPattern **/
import ArrayPattern from './patterns/array.js'

/** @typedef {from './patterns/object.js')} ObjectPattern **/
import ObjectPattern from './patterns/object.js'

/** @typedef {from './patterns/assignment.js')} AssignmentPattern **/
import AssignmentPattern from "./patterns/assignment.js"

// generators
/** @typedef {from './statements/statement.js')} Statement **/
import Statement from "./statements/statement.js"

/** @typedef {from './expressions/abstract/expression.js')} Expression **/
import Expression from "./expressions/abstract/expression.js";

// api and aliases
/** @typedef {from './statements/statementContainer')} StatementContainer  **/
import StatementContainer from "./statements/statementContainer.js";

/** @typedef {from './expressions/abstract/expressionContainer')} ExpressionContainer **/
import ExpressionContainer from "./expressions/abstract/expressionContainer.js";

// modules
/** @typedef {from './modules/specifier')} ModuleSpecifier  **/
import ModuleSpecifier from "./modules/specifier.js";

/** @typedef {from './modules/declaration')} ModulesDeclaration  **/
import ModuleDeclaration from "./modules/declaration.js";

/** @typedef {from './modules/import/declaration')} ImportDeclaration  **/
import ImportDeclaration from "./modules/import/declaration.js";

/** @typedef {from './modules/import/specifier')} ImportSpecifier  **/
import ImportSpecifier from "./modules/import/specifier.js";

/** @typedef {from './modules/import/all')} ImportAllDeclaration  **/
import ImportAllDeclaration from "./modules/import/all.js";

/** @typedef {from './modules/import/named')} ImportNamedSpecifier  **/
import ImportNamedSpecifier from "./modules/import/named.js";

/** @typedef {from './modules/import/default')} ImportDefaultSpecifier  **/
import ImportDefaultSpecifier from "./modules/import/default.js";

/** @typedef {from './modules/export/all')} ExportDefaultSpecifier  **/
import ExportAllDeclaration from "./modules/export/all.js";

/** @typedef {from './modules/export/default')} ExportDefaultSpecifier  **/
import ExportDefaultDeclaration from "./modules/export/default.js";

/** @typedef {from './modules/export/named')} ExportNamedSpecifier  **/
import ExportNamedDeclaration from "./modules/export/named.js";

/** @typedef {from './modules/export/specifier')} ExportSpecifier  **/
import ExportSpecifier from "./modules/export/specifier.js";

// statements
/** @typedef {from './statements/placeholderStatement')} PlaceholderStatement  **/
import PlaceholderStatement from "./statements/placeholderStatement.js";

/** @typedef {from './statements/ConcreteStatement')} ConcreteStatement  **/
import ConcreteStatement from "./statements/concreteStatement.js";

/** @typedef {from './statements/directive')} Directive  **/
import Directive from "./statements/directive.js";

/** @typedef {from './statements/block')} BlockStatement  **/
import BlockStatement from "./statements/block.js";

/** @typedef {from './statements/declaration')} Declaration  **/
import Declaration from "./statements/declaration.js";

/** @typedef {from './statements/declaration')} VariableDeclaration  **/
import VariableDeclaration from "./statements/variableDeclaration.js";

/** @typedef {from './statements/empty')} EmptyStatement  **/
import EmptyStatement from "./statements/empty.js";

/** @typedef {from './statements/expression')} ExpressionStatement **/
import ExpressionStatement from "./statements/expression.js";

/** @typedef {from './statements/function')} FunctionDeclaration  **/
import FunctionDeclaration from "./statements/function.js";

/** @typedef {from './statements/function.raw')} RawFunction  **/
import RawFunctionDeclaration from "./statements/function.raw.js";

/** @typedef {from './statements/if')} IfStatement **/
import IfStatement from "./statements/if.js";

/** @typedef {from './statements/try')} TryStatement  **/
import TryStatement from "./statements/try.js";

/** @typedef {from './statements/switch')} SwitchStatement **/
import SwitchStatement from "./statements/switch.js";

/** @typedef {from './statements/break')} BreakStatememt  **/
import BreakStatement from "./statements/break.js";

/** @typedef {from './statements/break')} ContinueStatement **/
import ContinueStatement from "./statements/continue.js";

/** @typedef {from './statements/return')} ReturnStatement  **/
import ReturnStatement from "./statements/return.js";

/** @typedef {from './statements/forIn')} ForInStatement  **/
import ForInStatement from "./statements/forIn.js";

/** @typedef {from './statements/forOf')} ForOfStatement  **/
import ForOfStatement from "./statements/forOf.js";

/** @typedef {from './statements/for')} For **/
import ForStatement from "./statements/for.js";

/** @typedef {from './statements/for')} While **/
import WhileStatement from "./statements/while.js";

/** @typedef {from './statements/for')} DoWhile **/
import DoWhileStatement from "./statements/doWhile.js";

// expressions abstracts
/** @typedef {from './expressions/abstract/concreteExpression')} ConcreteExpression  **/
import ConcreteExpression from "./expressions/abstract/concreteExpression.js";

/** @typedef {from './expressions/abstract/targetableExpression')} TargetableExpression  **/
import TargetableExpression from "./expressions/abstract/targetableExpression.js";

/** @typedef {from './expressions/abstract/literalLike')} LiteralLike  **/
import LiteralLike from "./expressions/abstract/literalLike.js";

/** @typedef {from './expressions/abstract/identifierLike')} IdentifierLike  **/
import IdentifierLike from "./expressions/abstract/identifierLike.js";

// expressions concretes
/** @typedef {from './expressions/placeholderExpression')} PlaceholderExpression **/
import PlaceholderExpression from "./expressions/placeholderExpression.js";

/** @typedef {from './expressions/literal.js')} Literal **/
import Literal from "./expressions/literal.js";

/** @typedef {from './expressions/identifier.js')} Identifier **/
import Identifier from "./expressions/identifier.js";

/** @typedef {from './expressions/spread')} Spread  **/
import SpreadElement from "./expressions/spread.js";

/** @typedef {from './expressions/array')} ArrayExpression  **/
import ArrayExpression from "./expressions/array.js";

/** @typedef {from './expressions/arrow')} ArrowExpression  **/
import ArrowFunctionExpression from "./expressions/arrow.js";

/** @typedef {from './expressions/arrow.raw')} RawArrowFunctionExpression  **/
import RawArrowFunctionExpression from "./expressions/arrow.raw.js";

/** @typedef {from './expressions/assignment')} AssignmentExpression  **/
import AssignmentExpression from "./expressions/assignment.js";

/** @typedef {from './expressions/unary')} UnaryExpression  **/
import UnaryExpression from "./expressions/unary.js";

/** @typedef {from './expressions/logical')} LogicalExpression  **/
import LogicalExpression from "./expressions/logical.js";

/** @typedef {from './expressions/binary')} BinaryExpression  **/
import BinaryExpression from "./expressions/binary.js";

/** @typedef {from './expressions/conditional')} ConditionalExpression  **/
import ConditionalExpression from "./expressions/conditional.js";

/** @typedef {from './expressions/object')} ObjectExpression  **/
import ObjectExpression from "./expressions/object.js";

/** @typedef {from './expressions/member')} MemberExpression  **/
import MemberExpression from "./expressions/member.js";

/** @typedef {from './expressions/function')} FunctionExpression  **/
import FunctionExpression from "./expressions/function.js";

/** @typedef {from './expressions/function.raw')} RawFunctionExpression  **/
import RawFunctionExpression from "./expressions/function.raw.js";

/** @typedef {from './expressions/call')} CallExpression **/
import CallExpression from "./expressions/call.js";

/** @typedef {from './expressions/new')} NewExpression **/
import NewExpression from "./expressions/new.js";

/** @typedef {from './expressions/await')} AwaitExpression **/
import AwaitExpression from "./expressions/await.js";

/** @typedef {from './expressions/sequence')} SequenceExpression **/
import SequenceExpression from "./expressions/sequence.js";

/** @typedef {from './expressions/taggedTemplate')} TaggedTemplateExpression **/
import TaggedTemplateExpression from "./expressions/taggedTemplate.js";

/** @typedef {from './expressions/templateLiteral')} TemplateLiteral **/
import TemplateLiteral from "./expressions/templateLiteral.js";

// functions
/** @typedef {from './lambda')} Lambda **/
import Lambda from "./lambda.js";

/** @typedef {from './function')} Function **/
import Function from "./function.js";

/** @typedef {from './program')} Program **/
import Program from "./program.js";

/** @typedef {from './class')} Class **/
import ClassDeclaration from "./statements/class.js";

//API and Aliases tools
import apiBroker from "./apiTools/apiBroker.js";

import apiBindings from "./apiTools/apiBindings.js";

//JSDOC

export {
  parse,
  Node,
  VirtualNode,
  Pattern,
  Property,
  RestElement,
  ArrayPattern,
  ObjectPattern,
  AssignmentPattern,
  Statement,
  ConcreteStatement,
  Expression,
  StatementContainer,
  ExpressionContainer,
  ModuleSpecifier,
  ModuleDeclaration,
  ImportDeclaration,
  ImportSpecifier,
  ImportAllDeclaration,
  ImportNamedSpecifier,
  ImportDefaultSpecifier,
  ExportAllDeclaration,
  ExportDefaultDeclaration,
  ExportNamedDeclaration,
  ExportSpecifier,
  PlaceholderStatement,
  Directive,
  BlockStatement,
  Declaration,
  VariableDeclaration,
  EmptyStatement,
  ExpressionStatement,
  FunctionDeclaration,
  RawFunctionDeclaration,
  IfStatement,
  TryStatement,
  SwitchStatement,
  BreakStatement,
  ContinueStatement,
  ReturnStatement,
  ForInStatement,
  ForOfStatement,
  ForStatement,
  WhileStatement,
  DoWhileStatement,
  ConcreteExpression,
  TargetableExpression,
  LiteralLike,
  IdentifierLike,
  PlaceholderExpression,
  Literal,
  Identifier,
  SpreadElement,
  ArrayExpression,
  ArrowFunctionExpression,
  RawArrowFunctionExpression,
  AssignmentExpression,
  UnaryExpression,
  LogicalExpression,
  BinaryExpression,
  ConditionalExpression,
  ObjectExpression,
  MemberExpression,
  FunctionExpression,
  RawFunctionExpression,
  CallExpression,
  NewExpression,
  AwaitExpression,
  SequenceExpression,
  TaggedTemplateExpression,
  TemplateLiteral,
  Lambda,
  Function,
  Program,
  ClassDeclaration,
  apiBroker,
  apiBindings

  //JSDOC
}

export * from "./extensions/index.js"