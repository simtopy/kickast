import * as Internal from './internal.js';

/**
 *
 *
 * @class Pattern
 * @extends {Internal.Node}
 */
class Pattern extends Internal.Node {

  constructor(parentNode) {
    super(parentNode)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
    }
  }
}
export default Pattern;