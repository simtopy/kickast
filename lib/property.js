import { PlaceholderTag } from './extensions/index.js';
import * as Internal from './internal.js';

/**
 *
 * @class Property
 * @extends {Internal.Node}
 * 
 */
class Property extends Internal.Node {
  constructor(node, key, value, kind, computed, method, shorthand) {
    super(node)
    this.comments = []

    // key
    if (typeof key === "string") {
      this.key = new Internal.Identifier(this, key)
    } else if (typeof key === "function") {
      this.key = this.apply(key, new Internal.PlaceholderExpression(this))
    } else this.key = key

    this.value = shorthand ? this.key : value
    this.kind = kind
    this.method = method
    this.computed = computed
    this.shorthand = shorthand
  }

  /**
   * 
   * @param {Internal.typeGeneratorCb} typeGeneratorCb 
   */
  Type(typeGeneratorCb) {
    this.jsDocType = new Internal.PlaceholderType()
    typeGeneratorCb(this.jsDocType)
    return this
  }

  /**
   * 
   * @param {String} com 
   */
  Describe(com) {
    this.jsDocComment = com
    return this
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      comments: this.comments,
      key: this.key.node,
      value: this.value.node,
      kind: this.kind,
      method: this.method,
      computed: this.computed,
      shorthand: this.shorthand
    }
  }
}
export default Property;