import * as Internal from './internal.js'

/**
 * @typedef {Object} ModuleAliases
 **/
/**
 * @typedef {Object} StatementAliases
 **/
/**
 * @typedef {Object} ExpressionAliases
 **/

/**
 * @typedef {Object} KAOptions - Object providing custom aliases for modules, statements or expressions, defined in sub objects
 * @property {ModuleAliases=} ModuleAliases - Custom aliases for modules
 * @property {StatementAliases=} StatementAliases - Custom aliases for statements
 * @property {ExpressionAliases=} ExpressionAliases - Custom aliases for Expressions
 * @property {boolean=} autoJsDoc - Autodocument the output using annotations features
 * @property {boolean=} logOutput - Log generated output
 * @property {boolean=} hints - Display Jsdoc hints
 * @property {boolean=} debug - Kickast debugging
 * @property {import('astring').generate=} generate
 * @property {any=} generateOptions
 **/

let astringGenerate = null
try {
  astringGenerate = (await import('astring')).generate
} finally { }

/**
 * @class Kickast
 * An instance of Kickast, with a defined set of parameters/options
 */
class Kickast {
  /**
   * 
   * @param {KAOptions} opts 
   */
  constructor(opts = {}) {
    this.options = {}
    this.options.generate = astringGenerate
    this.options.debug = process.env.KICKAST_DEBUG ? new Boolean(process.env.KICKAST_DEBUG) : false
    this.options.autoJsDoc = process.env.KICKAST_AUTOJSDOC ? new Boolean(process.env.KICKAST_AUTOJSDOC) : true
    this.options.logOutput = process.env.KICKAST_LOGOUTPUT ? new Boolean(process.env.KICKAST_LOGOUTPUT) : false
    this.options.jsDocHints = process.env.KICKAST_JSDOCHINTS ? new Boolean(process.env.KICKAST_JSDOCHINTS) : false
    this.options.generateOptions = {
      comments: true,
    }
    Object.assign(this.options, opts)
    if (this.options.ModuleAliases) Internal.apiBroker.bindObject(Internal.Program, this.options.ModuleAliases)
    if (this.options.StatementAliases) Internal.apiBroker.bindObject(Internal.StatementContainer, this.options.StatementAliases)
    if (this.options.ExpressionAliases) Internal.apiBroker.bindObject(Internal.Expression, this.options.ExpressionAliases)
  }

  /**
   * @method
   * @returns {Internal.Program}
   */
  Program() {
    return new Internal.Program(this)
  }

  generate(node, options) {
    return this.options.generate(node, options)
  }

  set generateFunction(gen) {
    this.options.generate = gen
  }
}

/**
 * @description Return a Program object builder with a defined set of parameter/options
 * @param {KAOptions?} opts
 * @returns {function(): Internal.Program}
 */
export function configurator(opts = {}) {
  const instance = new Kickast(opts)
  return () => instance.Program()
}