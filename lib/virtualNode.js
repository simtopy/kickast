import util from 'util';
import * as Internal from './internal.js';
import assert from 'assert';
import JsdocSequence from './extensions/jsdoc/base/jsdocSequence.js';
import { JSFunction } from './extensions/jsdoc/types/simples.js';

/** @typedef {import('./doc/callbacksAnnotations').Internal.StatementGeneratorCb} Internal.StatementGeneratorCb */
/**
 * A node that is out of the estree specification and don't appear in the final ast
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#node-objects} for specifications.
 *
 **/
class VirtualNode extends Internal.Node {
  constructor(parent) {
    super(parent)
    this.comments = false
  }
  /**
   * the node member is a canonical representation of a AST node, as described by the standard.
   * These informations will be used by the generators to produce the code.
   **/
  get _node() {
    //No type !
    return {}
  }
}

export default VirtualNode;