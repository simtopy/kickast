/* eslint-disable max-classes-per-file */
import * as Internal from '../internal.js';
import assert from 'assert';
import TypeTag from '../extensions/jsdoc/tags/type.js';

const VARKINDS = ["var", "let", "const"]

/**
 *
 * @class VariableDeclarator
 * @extends {Internal.Node}
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#variabledeclarator}
 */
class VariableDeclarator extends Internal.Node {
  constructor(node, id, init) {
    super(node)
    this.id = id
    this.init = init
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      id: this.id.node,
      init: this.init ? this.init.node : null,
    }
  }
}

/**
 *
 *
 * @class VariableDeclaration
 * @extends {Internal.Declaration}
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#variabledeclarationhttps://github.com/estree/estree/blob/master/es5.md#variabledeclarationhttps://github.com/estree/estree/blob/master/es5.md#variabledeclaration}
 */
class VariableDeclaration extends Internal.Declaration {
  constructor(node, kind) {
    super(node)

    // Test if declaration kind is valid, throw an error against failure.
    assert(VARKINDS.includes(kind), `${kind} is not a valid variable kind.`)

    this.kind = kind
    this.declarations = []
  }

  get node() {
    this.jsDocTags = new Internal.JsdocSequence(jsd => {
      if (this.jsDocComment) jsd.Raw(this.jsDocComment)

      const alreadyTypeAnnotated = this.jsDocTags ? this.jsDocTags.tags.find(t => t instanceof TypeTag) : false

      //User has set a type
      if (this.jsDocType) jsd.Type(this.jsDocType)
      //if hasnt beeen annotated by children
      else if (!alreadyTypeAnnotated)
        //If a type has been annoted on this declaration
        //If no type
        if (this.kickastInstance.options.jsDocHints) jsd.CustomTag("todo", "", "kickast-hint: you may add a JSDoc type annotation to this declaration")
    })
    return super.node
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      declarations: this.declarations.map(d => d.node),
      kind: this.kind
    }
  }

  /**
   * @private
   */
  get jsDocType() {
    return this._typeAnnotation
  }

  /**
   * @private
   */
  set jsDocType(type) {
    if (type === null) {
      this._typeAnnotation = (new PlaceholderType)
      this._typeAnnotation.Any
    }
    this._typeAnnotation = type
  }

  /* API */

  /**
   *
   * @param {String} identifierString
   * @param {Internal.ExpressionGeneratorCb} InitialValue
   * @returns
   * @memberof VariableDeclaration
   */
  Identifier(key, init) {
    // initializer
    if (typeof init === "function") {
      init = this.apply(init, new Internal.PlaceholderExpression(this))
    } else if (init !== undefined) {
      init = new Internal.Literal(this, init)
    } else init = null
    // create a new identifer
    let id = new Internal.Identifier(this, key)
    // create a new declarator
    this.declarations.push(new VariableDeclarator(this, id, init))
    // return the declaration for chain calls
    return this
  }

  /**
   * 
   * @param {String} key 
   * @param {Internal.ExpressionGeneratorCb} init 
   * @returns 
   */
  Id(key, init) {
    return this.Identifier(key, init)
  }

  /**
   *
   *
   * @param {Internal.ArrayPatternCb} pattern
   * @param {Internal.ExpressionContainer | string } init
   * @returns
   * @memberof VariableDeclaration
   */
  ArrayPattern(pattern, init) {
    if (typeof init === "function") {
      init = this.apply(init, new Internal.PlaceholderExpression(this))
    } else init = new Internal.Literal(this, init)
    let id = new Internal.ArrayPattern(this, pattern)
    // create a new declarator
    this.declarations.push(new VariableDeclarator(this, id, init))
    // return the declaration for chain calls
    return id
  }

  /**
   *
   *
   * @param {Internal.ObjectPatternCb} pattern
   * @param {Internal.ExpressionContainer | string} init
   * @returns
   * @memberof VariableDeclaration
   */
  ObjectPattern(pattern, init) {
    if (typeof init === "function") {
      init = this.apply(init, new Internal.PlaceholderExpression(this))
    } else init = new Internal.Literal(this, init)

    let id = new Internal.ObjectPattern(this, pattern)
    // create a new declarator
    this.declarations.push(new VariableDeclarator(this, id, init))
    // return the declaration for chain calls
    return id
  }

  set statement(s) {
    assert(s instanceof VariableDeclarator, "this.statement must be a variable declarator.")
    s.parent = this.concreteParent
    this._statement = s
    return s
  }

  /**
   * 
   * @param {Internal.typeGeneratorCb} typeGeneratorCb 
   */
  Type(typeGeneratorCb) {
    this.jsDocType = new Internal.PlaceholderType()
    typeGeneratorCb(this.jsDocType)
    return this
  }

  /**
   * 
   * @param {String} com 
   */
  Describe(com) {
    this.jsDocComment = com
    return this
  }
}

export default VariableDeclaration;