// eslint-disable-next-line max-classes-per-file
import * as Internal from '../internal.js';

/**
 *
 * @class SwitchCase
 * @extends {Internal.PlaceholderStatement}
 * @see{https://github.com/estree/estree/blob/master/es5.md#switchcase}
 **/
class SwitchCase extends Internal.StatementContainer {

  /**
   * 
   * @param {*} node 
   * @param {Internal.ExpressionGeneratorCb|Internal.ConcreteExpression} test 
   * @param {Internal.StatementGeneratorCb} consequent 
   */
  constructor(node, test, consequent) {
    super(node)
    this.comments = []
    if (typeof test === "function") {
      this.test = this.apply(test, new Internal.PlaceholderExpression(this))
    } else if (test) {
      this.test = new Internal.Literal(this, test)
    } else this.test = null
    this.consequent = []
    this.apply(consequent, this)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      test: this.test ? this.test.node : null,
      consequent: this.consequent.map(s => s.node)
    }
  }

  set statement(s) {
    this.consequent.push(s)
    return s
  }
}

/**
 *
 * @class SwitchStatement
 * @extends {Internal.Statement}
 * @see{@link https://github.com/estree/estree/blob/master/es5.md#switchstatement}
 */
class SwitchStatement extends Internal.ConcreteStatement {

  /**
   * 
   * @param {Internal.Node} node 
   * @param {Internal.IdentifierLike} discriminant 
   * @param {Internal.SwitchCb} callback 
   */
  constructor(node, discriminant, callback) {
    super(node)
    this.discriminant = this.apply(discriminant, new Internal.PlaceholderExpression(this))
    this.cases = []
    if (callback) this.apply(callback, this)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      discriminant: this.discriminant.node,
      cases: this.cases.map(c => c.node)
    }
  }

  /* SwitchStatement API */

  /**
   *
   * @param {Internal.PlaceholderExpression} test
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   **/
  Case(test, cb) {
    this.cases.push(new SwitchCase(this, test, cb))
    return this
  }

  /**
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   **/
  Default(cb) {
    this.cases.push(new SwitchCase(this, null, cb))
    return this
  }

}

export default SwitchStatement;