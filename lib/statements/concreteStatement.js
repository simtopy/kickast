import * as Internal from "../internal.js";

class ConcreteStatement extends Internal.Statement {
  constructor(...args) {
    super(...args)
    this.comments = []
  }

  get _node() {
    return {
      type: this.constructor.name,
      comments: this.comments
    }
  }
}

export default ConcreteStatement