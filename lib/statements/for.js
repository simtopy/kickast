import * as Internal from '../internal.js'
/**
 *
 *
 * 
 * @class ForStatement
 * @extends {Internal.ConcreteStatement}
 * 
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#forstatement}
 */
class ForStatement extends Internal.ConcreteStatement {

  constructor(parentNode, iter, init, test, update) {
    super(parentNode)
    this.init = new Internal.VariableDeclaration(this, "let").Identifier(iter, init)
    this.test = this.apply(test, new Internal.PlaceholderExpression(this))
    this.update = this.apply(update, new Internal.PlaceholderExpression(this))
    this.body = null
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      init: this.init.node,
      test: this.test.node,
      update: this.update.node,
      body: this.body.node
    }
  }
  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   * @memberof ForStatement
   */
  Do(cb) {
    this.body = this.apply(cb, new Internal.PlaceholderStatement(this))
    return this
  }
  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   * @memberof ForStatement
   */
  DoBlock(cb) {
    this.body = this.apply(cb, new Internal.BlockStatement(this))
    return this
  }
}

export default ForStatement