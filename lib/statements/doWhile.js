import * as Internal from '../internal.js';

/**
 *
 *
 * 
 * @class DoWhileStatement
 * @extends {Internal.Statement}
 * 
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#dowhilestatement}
 */
class DoWhileStatement extends Internal.ConcreteStatement {

  constructor(parentNode, body) {
    super(parentNode)
    this.test = null
    this.body = this.apply(body, new Internal.BlockStatement(this))
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      test: this.test.node,
      body: this.body.node
    }
  }
  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   * @memberof WhileStatement
   */
  While(test) {
    this.test = this.apply(test, new Internal.PlaceholderExpression(this))
    return this
  }
}

export default DoWhileStatement