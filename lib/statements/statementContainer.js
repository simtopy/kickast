import * as Internal from '../internal.js';

/**
 * 
 * This class offers the statement creation API to child classes (Block, Program)
 * 
 * @class StatementContainer
 * @extends {Internal.Node}
 * 
 *
 **/
class StatementContainer extends Internal.Statement {

  get _node() {
    return super._node
  }

  /**
   * set the concrete underlying statement
   **/
  set statement(s) {
    throw Error("Shall be overriden")
  }

  /**
   * get the concrete underlying statement
   **/
  get statement() {
    return this._statement
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Statements API
  ///////////////////////////////////////////////////////////////////////////////

  /**
   * Make the current statement an empty statement
   **/
  Empty() {
    return this.statement = new Internal.EmptyStatement(this)
  }

  /**
   * Make the current statement a JS directive
   *
   * @param {String} str - Directive to add
   *
   **/
  Directive(str) {
    return this.statement = new Internal.Directive(this, str)
  }

  /**
   * Make the current statement an expression statement.
   * 
   * @returns {Internal.ExpressionStatement}
   **/
  ExpressionStatement() {
    return this.statement = new Internal.ExpressionStatement(this)
  }

  /**
   * Make the current statement a block.
   *
   * @param {Internal.StatementGeneratorCb} callback - actions to be performed inside the block
   *
   * @example
   * <code><pre>
   * pgrm.Block(b => {
   *    b.Let("foo","bar");
   * })</code></pre>
   * 
   *
   **/
  Block(callback) {
    return this.statement = new Internal.BlockStatement(this, callback)
  }

  /**
   * Make this statement a variable declaration
   *
   * @param {String} kind - type of declaration. must be a valid var kind.
   *
   **/
  Declare(kind = "let") {
    return this.statement = new Internal.VariableDeclaration(this, kind)
  }

  /**
   * Make this statement is a raw (or inline) function
   *
   * @param {Function} f - the function to be declared
   **/
  RawFunction(f) {
    return this.statement = new Internal.RawFunctionDeclaration(this, f)
  }

  /**
   * Make this statement a Function declaration
   *
   * @param {String} name - the name of the function
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body - definition of the function body
   * 
   *
   **/
  Function(name, params, body) {
    return this.statement = new Internal.FunctionDeclaration(this, false, name, params, body)
  }

  /**
   * Make this statement a asynchrone function declaration
   * 
   * @param {String} name - name of the Function
   * @param {Internal.paramsGenerator} params - Parameters of the Finction
   * @param {Internal.StatementGeneratorCb} body - The body of the function
   * 
   * 
   **/
  AsyncFunction(name, params, body) {
    return this.statement = new Internal.FunctionDeclaration(this, true, name, params, body)
  }

  /**
   * 
   * @param {String} name - name of the class 
   * @param {Internal.ClassGeneratorCb} callback - Class content specifier
   */
  Class(name, callback) {
    return this.statement = new Internal.ClassDeclaration(this, name, callback)
  }

  /**
   * Make this statement an if statement
   *
   * @param {Internal.ExpressionGeneratorCb} test - the test to be performed
   * 
   **/
  If(test) {
    return this.statement = new Internal.IfStatement(this, test)
  }

  /**
   * Make this statement a Switch statement
   * 
   * @param {Internal.ExpressionGeneratorCb} discriminant - the value to be tested agains
   * @param {Internal.SwitchGeneratorCb} callback - callback for the case statements
   *
   **/
  Switch(discriminant, callback) {
    return this.statement = new Internal.SwitchStatement(this, discriminant, callback)
  }

  /**
   * Make this statement a Break statement
   *
   **/
  BreakStatement(label) {
    return this.statement = new Internal.BreakStatement(this, label)
  }

  /**
   * Make this statement a continue statement 
   *
   */
  ContinueStatement(label) {
    return this.statement = new Internal.ContinueStatement(this, label)
  }

  /**
   * Make this statement a return statement
   *
   * @param {Internal.ExpressionGeneratorCb=} exp - the expression which will be evaluated and returned
   *
   **/
  ReturnStatement(exp) {
    return this.statement = new Internal.ReturnStatement(this, exp)
  }
  /**
   *
   * Make this statement a try statement
   * 
   * @param {Internal.StatementGeneratorCb} cb - callback function defining the body of the try block
   *
   */
  Try(cb) {
    return this.statement = new Internal.TryStatement(this, cb)
  }

  /**
   * Loop through the properties of an Object.
   * 
   * The key won't be in the scope of the function
   *
   * @param {String} key - The key to use
   * @param {Internal.ExpressionGeneratorCb} right - Inline structure or identifier
   * @param {Internal.StatementGeneratorCb} body - Callback function to perform actions
   * @returns {Internal.ForInStatement} - The resulting statement
   * @memberof StatementContainer
   * 
   * @example 
   * this.prgm.ForIn("key", e => e.Get("array", b => {
   *   //body statements
   * }))
   * 
   */
  ForIn(key, right, body) {
    return this.statement = new Internal.ForInStatement(this, key, true, right, body)
  }

  /**
   * Loop through the properties of an Object.
   * 
   * The key won't be in the scope of the function
   *
   * @param {String} key - The key to use
   * @param {Internal.ExpressionGeneratorCb} right - Inline structure or identifier
   * @param {Internal.StatementGeneratorCb} body - Callback function to perform actions
   * @returns {Internal.ForInStatement} - The resulting statement
   * @memberof StatementContainer
   * 
   * @example 
   * this.prgm.ForIn("key", e => e.Get("array", b => {
   *   //body statements
   * }))
   * 
   */
  ForAwaitIn(key, right, body) {
    return this.statement = new Internal.ForInStatement(this, key, true, right, body, true)
  }

  /**
   * Loop through the properties of an Object.
   * 
   * The key will be in the scope of the function
   *
   * @param {String} key - The key to use
   * @param {Internal.ExpressionGeneratorCb} right - Inline structure or identifier
   * @param {Internal.StatementGeneratorCb} body - Callback function to perform actions
   * @returns {Internal.ForInStatement} The resulting statement
   * @memberof StatementContainer
   * 
   * @example 
   * this.prgm.ForIn("key", e => e.Get("array", b => {
   *   //body statements
   * }))
   * 
   */
  ForLetIn(key, right, body) {
    return this.statement = new Internal.ForInStatement(this, key, false, right, body)
  }

  /**
   * Loop through the values of an iterable
   * 
   * The key won't be in the scope of the function
   *
   * @param {String} key - the key to use
   * @param {Internal.ExpressionGeneratorCb} right - Inline structure or identifier
   * @param {Internal.StatementGeneratorCb} body - Callback function to perform actions
   * @returns {Internal.ForOfStatement} - The resulting statemente
   * @memberof StatementContainer
   * 
   * @example 
   * this.prgm.ForOF("key", e => e.Get("array", b => {
   *   //body statements
   * }))
   * 
   */
  ForOf(key, right, body) {
    return this.statement = new Internal.ForOfStatement(this, key, true, right, body)
  }

  /**
   * Loop through the values of an iterable
   * 
   * The key won't be in the scope of the function
   *
   * @param {String} key - the key to use
   * @param {Internal.ExpressionGeneratorCb} right - Inline structure or identifier
   * @param {Internal.StatementGeneratorCb} body - Callback function to perform actions
   * @returns {Internal.ForOfStatement} - The resulting statemente
   * @memberof StatementContainer
   * 
   * @example 
   * this.prgm.ForOF("key", e => e.Get("array", b => {
   *   //body statements
   * }))
   * 
   */
  ForAwaitOf(key, right, body) {
    return this.statement = new Internal.ForOfStatement(this, key, true, right, body, true)
  }

  /**
   * Loop through the values of an iterable
   * 
   * The key will be in the scope of the function
   *
   * @param {String} key - the key to use
   * @param {Internal.Identifier} right - Identifier of the structure
   * @param {*} body - Callback function to perform actions
   * @returns {Internal.ForOfStatement} - The resulting statement
   * @memberof StatementContainer
   * 
   * @example 
   * this.prgm.ForOF("key", e => e.Get("array", b => {
   *   //body statements
   * }))
   **/
  ForLetOf(key, right, body) {
    return this.statement = new Internal.ForOfStatement(this, key, false, right, body)
  }

  /**
   * @param {string} iter
   * @param {number} init
   * @param {Internal.ExpressionGeneratorCb} test
   * @param {Internal.ExpressionGeneratorCb} update
   */
  For(iter, init, test, update) {
    return this.statement = new Internal.ForStatement(this, iter, init, test, update)
  }

  /**
   *
   * @param {Internal.ExpressionGeneratorCb} test
   * @memberof StatementContainer
   */
  While(test) {
    return this.statement = new Internal.WhileStatement(this, test)
  }

  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof StatementContainer
   */
  Do(body) {
    return this.statement = new Internal.DoWhileStatement(this, body)
  }

  ///////////////////////////////////////////////////////////////////////////////
  // Statements API Aliases
  ///////////////////////////////////////////////////////////////////////////////

  /**
   * Make this statement a break statement.
   * 
   * @returns {Internal.BreakStatement} - The resulting statement
   * 
   **/
  Break() {
    return this.BreakStatement()
  }

  /**
   * Make this statement a return statement
   * 
   * @param {Internal.ExpressionGeneratorCb=} exp - function defining the return expression
   * @returns {(Internal.PlaceholderExpression|Internal.ConcreteExpression)} Resulting statement
   **/
  Return(exp) {
    return this.ReturnStatement(exp).argument
  }

  /**
   * Make this statement an Expression Statement
   * 
   * The expression nature may be precised using the expression creation API @see ExpressionContainer
   * 
   * @returns {Internal.PlaceholderExpression} A placeholder expression
   * 
   **/
  Expression() {
    return this.ExpressionStatement().expression
  }

  /**
   * Make this statement an Identifier expression statement
   * 
   * @param {string} name - name of the identifier
   * @param  {...String} members - members accession chain
   * 
   * @returns {Internal.Identifier} the resulting identifier
   **/
  Identifier(name, ...members) {
    return this.Expression().Identifier(name, ...members)
  }

  /**
   * Make this statement a This expression statement
   * 
   * @param  {...String} members - members accession chain
   * @returns {Internal.Identifier} The resulting identifier
   * 
   * @example s->s.This("m1","m2","m3")
   **/
  This(...members) {
    return this.Expression().This(...members)
  }

  /**
   * Make this statement a Super memberExpression statement
   * @param {...String} members 
   * @returns {Internal.Identifier} The resulting Identifier
   */
  Super(...members) {
    return this.Expression().Super(...members)
  }

  /**
   * 
   * Make this statement a Let variable declaration
   * 
   * @param {String} key  - name of the variable
   * @param {String | Internal.ExpressionGeneratorCb} init - initial value of the variable
   * @return {Internal.VariableDeclaration}
   * 
   */
  Let(key, init) {
    return this.Declare("let").Identifier(key, init)
  }

  /**
   * 
   * Make this statement a Var variable declaration
   * 
   * @param {String} key  - name of the variable
   * @param {String | Internal.ExpressionGeneratorCb} init - initial value of the variable
   * @returns {Internal.VariableDeclaration}
   * 
   */
  Var(key, init) {
    return this.Declare("var").Identifier(key, init)
  }

  /**
   * 
   * Make this statement a Const variable declaration
   * 
   * @param {String} key  - name of the variable
   * @param {String | Internal.ExpressionGeneratorCb} init - initial value of the variable
   * @returns {Internal.VariableDeclaration}
   * 
   */
  Const(key, init) {
    return this.Declare("const").Identifier(key, init)
  }

  /**
   * Declare an array using a let declaration
   * 
   * @param {Internal.ArrayPatternCb} elements - Elements to declare
   * @param {String[] | Internal.ExpressionGeneratorCb[]} init - Initial values
   * @returns {Internal.ArrayPattern}
   * 
   * @example 
   * this.prgm.LetArray(pattern => {
   *   pattern
   *     .Identifier("a")
   *     .Identifier("b", e => e.Null())
   *     .Rest("c")
   * }, [1, 2, 3, 4, 5])
   * 
   */
  LetArray(elements, init) {
    return this.Declare("let").ArrayPattern(elements, init)

  }

  /**
   * Declare an array using a var declaration and a patterrn
   * 
   * @param {Internal.ArrayPatternCb} elements - Elements to declare
   * @param {String[] | Internal.ExpressionGeneratorCb[]} init - Initial values
   * @returns {Internal.ArrayPattern}
   */
  VarArray(elements, init) {
    return this.Declare("var").ArrayPattern(elements, init)
  }
  /**
   * Declare an array using a const declaration and a pattern
   * 
   * @param {Internal.ArrayPatternCb} elements  - Elements to declare
   * @param {String[] | Internal.ExpressionGeneratorCb[]} init - Initial values
   * @returns {Internal.ArrayPattern}
   */
  ConstArray(elements, init) {
    return this.Declare("const").ArrayPattern(elements, init)
  }

  /**
   * Declare an object using a let declaration and a pattern
   * 
   * 
   * @param {Internal.ObjectPatternCb} properties - Defining object properties, using a callback
   * @param {Internal.ExpressionGeneratorCb[]} init - defining initial values by object declaration
   * @returns {Internal.ObjectPattern}
   * 
   * @example 
   * prgm.LetPattern(p => {
   *   p.Property("foo")
   *   p.Property("bar",21)
   * 
   * }, { foo: 21 }
   * )
   **/
  LetPattern(properties, init) {
    return this.Declare("let").ObjectPattern(properties, init)
  }

  /**
   * Declare an object using a var declaration and properties
   *
   * @param {Internal.ObjectPatternCb} properties - Defining object properties, using a callback
   * @param {Internal.ExpressionGeneratorCb[]} init - defining initial values
   * @returns {Internal.ObjectPattern}
   * @memberof StatementContainer
   */
  VarPattern(properties, init) {
    return this.Declare("var").ObjectPattern(properties, init)
  }

  /**
   * Declare an object using a const declaration and properties
   *
   * @param {Internal.ObjectPatternCb} properties - Defining object properties, using a callback
   * @param {Internal.ExpressionGeneratorCb[]} init - defining initial values
   * @returns {Internal.ObjectPattern}
   * @memberof StatementContainer
   */
  ConstPattern(properties, init) {
    return this.Declare("const").ObjectPattern(properties, init)
  }

  /**
   * Make this statement an await expression statement
   * 
   * @see ExpressionContainer#Await()
   * @returns {Internal.AwaitExpression}
   * 
   **/
  Await() {
    return this.Expression().Await()
  }

  /**
   * Variable access
   *
   * @param {String} key
   * @param {String[]} members
   * @returns {Internal.Identifier}
   * @memberof StatementContainer
   * 
   * @deprecated - Use Id() instead
   * 
   */
  Get(key, ...members) {
    return this.Expression().Get(key, ...members)
  }

  /**
   * Variable access
   *
   * @param {String} key
   * @param {String[]} members
   * @returns {Internal.Identifier}
   * @memberof StatementContainer
   * 
   *
   * 
   */
  Id(key, ...members) {
    return this.Expression().Get(key, ...members)
  }

  /**
   * Variable setting
   *
   * @param {String} key
   * @param {String[]} members
   * @returns {Internal.AssignmentExpression}
   * @memberof StatementContainer
   */
  Set(key, ...members) {
    return this.Expression().Set(key, ...members)
  }

  /**
   * Make this statement a Number declaration
   * 
   * @param {String} name - name of the variable
   * @param {String} value  - value
   */
  Number(name, value = 0) {
    return this.Declare().Id(name, "number", e => e.Value(value))
  }

  /**
   * Make this statement a String declaration
   * 
   * @param {String} name - name of the variable
   * @param {String} value - value of the variable 
   */
  String(name, value = "") {
    return this.Declare().Id(name, "string", e => e.Value(value))
  }

  /**
   * Make this statement an Object declaration
   * 
   * @param {string} name - name of the variable
   * @param {string} value - value of the variable
   */
  Object(name, value = {}) {
    return this.Declare().Id(name, "object", e => e.Value(value))
  }

  /**
   * Make this statement a Bool declaration
   * 
   * @param {string} name - name of the variable
   * @param {boolean} value - value of the variable
   */
  Bool(name, value = false) {
    return this.Declare().Id(name, "boolean", e => e.Value(value))
  }

  /**
   * Make this statement a Bool declaration
   * 
   * @param {string} name - name of the variable
   * @param {any[]} value - value of the variable
   */
  Array(name, ...values) {
    let initExpression = null
    //return this.Declare().Id(name,"array", e=>e.Array().Literals(...values))
    this.Declare().Id(name, "array", e => initExpression = e.Array())
    if (values.length) initExpression.Literals(...values)
    return initExpression
  }
}

export default StatementContainer;