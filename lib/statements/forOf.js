import * as Internal from '../internal.js';

/**
 *
 *
 * @class ForOfStatement
 * @extends {Internal.ForInStatement}
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#forofstatement}
 */
class ForOfStatement extends Internal.ForInStatement {
  constructor(node, key, exists, right, body, awaitp=false) {
    super(node, key, exists, right, body, awaitp);
  }
}
export default ForOfStatement;