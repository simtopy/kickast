import * as Internal from '../internal.js';

/**
 * A JS Directive AST representation
 *
 * @class Directive
 * @extends {Internal.Statement}
 * 
 */
class Directive extends Internal.ConcreteStatement {

  constructor(node, directive) {
    super(node);
    this.directive = directive;
    this.expression = new Internal.PlaceholderExpression(this).Literal(directive);
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      expression: this.expression.node,
      directive: this.directive
    }
  }
}
export default Directive;