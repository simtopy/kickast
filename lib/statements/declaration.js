import * as Internal from '../internal.js';
import assert from 'assert';

/**
 *
 * @class Declaration
 * @extends {Internal.Statement}
 */
class Declaration extends Internal.ConcreteStatement {
  constructor(parentNode) {
    super(parentNode)
  }

  /**
 * 
 * @param {Internal.typeGeneratorCb} typeGeneratorCb 
 */
  Type(typeGeneratorCb) {
    this.jsDocType = new Internal.PlaceholderType()
    typeGeneratorCb(this.jsDocType)
    return this
  }

  /**
   * 
   * @param {String} com 
   */
  Describe(com) {
    this.jsDocComment = com
    return this
  }
}

export default Declaration;