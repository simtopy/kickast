import * as Internal from '../internal.js';

/**
 *
 *
 * @class FunctionDeclaration
 * @extends {Internal.Statement}
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#functiondeclaration}
 */
class FunctionDeclaration extends Internal.Declaration {
  constructor(node, async, name, params, body) {
    super(node);
    this.function = new Internal.Function(this, async, name, params, body)
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      ...this.function.node
    }
  }
}
export default FunctionDeclaration;