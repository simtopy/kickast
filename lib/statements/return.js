import * as Internal from '../internal.js';

/**
 *
 * @class ReturnStatement
 * @extends {Internal.Statement}
 * 
 * @see{@link https://github.com/estree/estree/blob/master/es5.md#returnstatement}
 */
class ReturnStatement extends Internal.ConcreteStatement {
  constructor(node, callback) {
    super(node)
    this.argument = new Internal.PlaceholderExpression(this)
    if (typeof callback === "function") this.apply(callback, this.argument)
    else if (callback !== undefined) this.argument.Literal(callback)
  }

  get _node() {
    const supernode = super._node
    const res = {
      ...supernode,
      argument: this.argument.node
    }
    return res
  }

  set expression(e) {
    this.argument = e;
    return e;
  }

  /**
   * 
   * @param {Internal.typeGeneratorCb} typeGeneratorCb 
   */
  Type(typeGeneratorCb) {
    this.argument.jsDocType = new Internal.PlaceholderType()
    typeGeneratorCb(this.argument.jsDocType)
    return this
  }

}

export default ReturnStatement;