import * as Internal from '../internal.js';

/**
 *
 *
 * @class ForInStatement
 * @extends {Internal.Statement}
 * 
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#forinstatement}
 */
class ForInStatement extends Internal.ConcreteStatement {

  constructor(node, name, exists, right, body, awaitp = false) {
    super(node)
    if (exists) {
      this.left = new Internal.Identifier(this, name);
    } else {
      this.left = new Internal.VariableDeclaration(this, "let")
      this.left.Identifier(name)
    }

    this.right = this.apply(right, new Internal.PlaceholderExpression(this));
    this.body = new Internal.BlockStatement(this, body);
    this.await = awaitp
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      left: this.left.node,
      right: this.right.node,
      body: this.body.node,
      await: this.await
    }
  }
}

export default ForInStatement;