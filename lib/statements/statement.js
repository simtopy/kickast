import * as Internal from '../internal.js';

/**
 * An AST Node that is a statement.
 * 
 * The statement could be concrete or abstract (undefined).
 * @class Statement
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#statements}
 * @extends {Internal.Node}
 **/
class Statement extends Internal.Node {

  /**
   * builds a statement.
   *
   * @param node - the parent Node  @see{@link Node}
   *
   **/
  constructor(parentNode) {
    super(parentNode)
    this.comments = []
    this._statement = this
  }

  get concreteParent() {
    if (this.trueParent === null) return null
    else if (this.trueParent instanceof Internal.PlaceholderStatement) return this.trueParent.concreteParent
    else return this.trueParent
  }

  /**
   * the statement node. will throw an error if the current object statement is abstract
   * (with no underlying concrete statement)
   *
   * user SHOULD test if the statement is defined/concrete before accessing this member.
   *
   * @see Node.node

  *
   **/
  get _node() {
    return super._node
  }

}

export default Statement;