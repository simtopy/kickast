/* eslint-disable default-param-last */
// eslint-disable-next-line max-classes-per-file
import { UV_FS_O_FILEMAP } from 'constants';
import * as Internal from '../internal.js';

/**
 *
 * @class ClassBody
 * @extends {Internal.Node}
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#classbody}
 */
class ClassBody extends Internal.Node {
  constructor(node, body = []) {
    super(node)
    this.body = body
  }

  get _node() {
    return {
      type: this.constructor.name,
      body: this.body.map(s => s.node)
    }
  }
}

/**
 *
 *
 * @class PrivateIdentifier
 * @extends {Internal.Node}
 * @see{@link https://github.com/estree/estree/blob/master/es22.md#privateIdentifier}
 */
class PrivateIdentifier extends Internal.Node {
  constructor(node, name) {
    super(node)
    this.name = name
  }

  get _node() {
    return {
      type: this.constructor.name,
      name: this.name,
    }
  }
}

/**
 *
 *
 * @class MethodDefinition
 * @extends {Internal.Declaration}
 * @see{@link https://github.com/estree/estree/blob/master/es5.md#methoddefinition}
 */
class MethodDefinition extends Internal.Declaration {
  constructor(node, kind, isStatic, computed, key, isAsync, params = [], body) {
    super(node)
    this.kind = kind
    this.static = isStatic
    this.computed = computed
    this.comments = []
    if (!key) this.key = null
    else if (key[0] !== '#') this.key = new Internal.Identifier(this, key)
    else this.key = new PrivateIdentifier(this, key.slice(1))

    this.value = new Internal.FunctionExpression(this, isAsync, params, body)

    //JSDOC
    const jsDoc = (jsd) => {
      if (this.static) jsd.Static()
      if (this.kind === "constructor") jsd.Constructor()
      if (this.kind === "method") jsd.Method()
      if (this.kind === "get") jsd.Getter()
      if (this.kind === "set") jsd.Setter()
    }
    this.jsDocTags = new Internal.JsdocSequence(jsDoc)
  }

  get _node() {
    return {
      type: this.constructor.name,
      kind: this.kind,
      static: this.static,
      computed: this.computed,
      key: this.key ? this.key.node : null,
      value: this.value ? this.value.node : null,
      comments: this.comments
    }
  }

  Public() {
    this.jsDocTags = new Internal.JsdocSequence(jsd => jsd.Public())
    return this
  }
  Private() {
    this.jsDocTags = new Internal.JsdocSequence(jsd => jsd.Private())
    return this
  }
  Protected() {
    this.jsDocTags = new Internal.JsdocSequence(jsd => jsd.Protected())
    return this
  }
}

/**
 *
 *
 * @class PropertyDefinition
 * @extends {Internal.Node}
 * @see{@link https://github.com/estree/estree/blob/master/es2022.md#propertydefinition}
 */
class PropertyDefinition extends Internal.Node {
  constructor(node, isStatic, computed, key, value) {
    super(node)
    this.static = isStatic
    this.computed = computed
    this.comments = []
    this.key = key ? new Internal.Identifier(this, key) : null
    // handle the key
    if (!key) this.key = null
    else if (key[0] !== '#') this.key = new Internal.Identifier(this, key)
    else this.key = new PrivateIdentifier(this, key.slice(1))
    // handle the value
    this.value = (typeof init === "function") ?
      this.apply(value, new Internal.PlaceholderExpression(this)) : init
  }

  get _node() {
    return {
      type: this.constructor.name,
      key: this.key ? this.key.node : null,
      value: this.value ? this.value.node : null,
      computed: this.computed,
      static: this.static,
      comments: this.comments
    }
  }
}

/**
 *
 *
 * @class ClassDeclaration
 * @extends {Internal.Declaration}
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#classdeclaration}
 */
class ClassDeclaration extends Internal.Declaration {

  /**
   * Creates an instance of ClassDeclaration.
   * @param {Internal.Node} node
   * @param {string} name
   * @param {Internal.ClassGeneratorCb} callback
   * @memberof ClassDeclaration
   */
  constructor(node, name, callback) {
    super(node);
    this.id = name ? new Internal.Identifier(this, name) : null
    this.superClass = null
    this.comments = []
    this.body = []
    if (typeof callback === "function") this.apply(callback, this)

    const jsDoc = (jsd) => {
      if (this.jsDocComment !== "") {
        jsd.Raw(this.jsDocComment)
      }
      jsd.Class()
      if (this.superClass) {
        jsd.Extends(this.superClass.name)
      }
    }
    this.jsDocTags = new Internal.JsdocSequence(jsDoc)
  }

  get _node() {
    const superNode = super._node
    return {
      ...superNode,
      id: this.id ? this.id.node : null,
      superClass: this.superClass ? this.superClass.node : null,
      body: (new ClassBody(this, this.body)).node,
      comments: this.comments
    }
  }

  set statement(s) {
    this.body.push(s)
    return s
  }

  /**
   *
   *
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof ClassDeclaration
   */
  Constructor(params, body) {
    const v = new MethodDefinition(this, "constructor", false, false, "constructor", false, params, body)
    this.statement = v
    return v
  }

  Describe(com) {
    this.jsDocComment = com
  }

  /**
   *
   *
   * @param {*} superclass
   * @returns
   * @memberof ClassDeclaration
   */
  Extends(superclass) {
    this.superClass = new Internal.Identifier(this, superclass)
    return this
  }

  /**
   *
   *
   * @param {String} name
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof ClassDeclaration
   */
  Method(name, params, body) {
    const v = new MethodDefinition(this, "method", false, false, name, false, params, body)
    this.statement = v
    return v
  }

  /**
   *
   *
   * @param {String} name
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof ClassDeclaration
   */
  AsyncMethod(name, params, body) {
    const v = new MethodDefinition(this, "method", false, false, name, true, params, body)
    return v
  }

  /**
   *
   *
   * @param {String} name
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof ClassDeclaration
   */
  StaticMethod(name, params, body) {
    const v = new MethodDefinition(this, "method", true, false, name, false, params, body)
    this.statement = v
    return v
  }

  /**
   *
   *
   * @param {String} name
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof ClassDeclaration
   */
  StaticAsyncMethod(name, params, body) {
    const v = new MethodDefinition(this, "method", true, false, name, true, params, body)
    this.statement = v
    return v
  }

  /**
   *
   *
   * @param {String} name
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof ClassDeclaration
   */
  Getter(name, body) {
    const v = new MethodDefinition(this, "get", false, false, name, false, [], body)
    this.statement = v
    return v
  }

  /**
   *
   *
   * @param {String} name
   * @param {Internal.paramsGenerator} params
   * @param {Internal.StatementGeneratorCb} body
   * @returns
   * @memberof ClassDeclaration
   */
  Setter(name, params, body) {
    const v = new MethodDefinition(this, "set", false, false, name, false, params, body)
    this.statement = v
    return v
  }

}
export default ClassDeclaration;