import * as Internal from '../internal.js';

/**
 * An IfStatement is the base structure for conditional branching / case disjunction in javascript
 *
 * This class provides an implementation compliant with ES5 Standard.
 * @see {@link  https://github.com/estree/estree/blob/master/es5.md#statements}
 *
 **/
class IfStatement extends Internal.ConcreteStatement {
  /**
   * @param node - The Parent node @see{@link Node}
   * @param {expressionGeneratonCb }test - a function that will create the boolean expression that will be tested. using the Expression api
   **/
  constructor(parentNode, test) {
    super(parentNode)
    this.test = new Internal.PlaceholderExpression(this)
    if (typeof test === "function") this.apply(test, this.test)
    this.consequent = null
    this.alternate = null
  }

  /**
   * AST node formal description
   **/
  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      test: this.test.node,
      consequent: this.consequent.node,
      alternate: this.alternate ? this.alternate.node : null
    }
  }

  /* IfStatement API */

  /**
   * Define the consequent action if the test succeed, using the statement API
   * under the hood.
   * 
   * @param {Internal.StatementGeneratorCb} cb
   *
   * @example
   * Then(s => s.Set("result"))
   * @example
   * Then(s => s.Identifier("console","log").Call("Success"))
   *
   * @returns this for purpose of chaining
   *
   **/
  Then(cb) {
    this.consequent = new Internal.PlaceholderStatement(this)
    this.apply(cb, this.consequent)
    return this
  }

  /**
   * Define the consequent actions if the test succeed
   * 
   * @param {Internal.StatementGeneratorCb} cb
   *
   * @see Then
   *
   **/
  ThenBlock(cb) {
    this.consequent = new Internal.BlockStatement(this)
    this.apply(cb, this.consequent)
    return this
  }

  /**
   * Define the alternate action, if the test fails
   * 
   * @param {Internal.StatementGeneratorCb} cb
   *
   * @see Then
   *
   **/
  Else(cb) {
    this.alternate = new Internal.PlaceholderStatement(this)
    this.apply(cb, this.alternate)
    return this
  }

  /**
   * Define the alternate actions if the test fails
   * 
   * @param {Internal.StatementGeneratorCb} cb
   *
   * @see Then
   **/
  ElseBlock(cb) {
    this.alternate = new Internal.BlockStatement(this)
    this.apply(cb, this.alternate)
    return this
  }

}

export default IfStatement;