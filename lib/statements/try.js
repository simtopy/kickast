import * as Internal from '../internal.js';

/**
 *
 * @class CatchClause
 * @extends {Internal.Node}
 * 
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#catchclause}
 * 
 */
class CatchClause extends Internal.Node {

  constructor(node, param, body) {
    super(node);
    this.param = this.apply(param, new Internal.PlaceholderExpression(this));
    this.body = new Internal.BlockStatement(this, body);
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      param: this.param.node,
      body: this.body.node
    }
  }

}

/**
 *
 *
 * @class TryStatement
 * @extends {Internal.Statement}
 * 
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#trystatement}
 */
class TryStatement extends Internal.ConcreteStatement {

  constructor(node, block) {
    super(node);
    this.block = new Internal.BlockStatement(this, block);
    this.handler = null;
    this.finalizer = null;
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      block: this.block.node,
      handler: this.handler ? this.handler.node : null,
      finalizer: this.finalizer ? this.finalizer.node : null
    }
  }

  /* TryStatement API */

  /**
   *
   *
   * @param {Internal.ExpressionGeneratorCb} param
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   * @memberof TryStatement
   */
  Catch(param, cb) {
    this.handler = new CatchClause(this, param, cb);
    return this;
  }

  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   * @memberof TryStatement
   */
  Finally(cb) {
    this.finalizer = new Internal.BlockStatement(this, cb);
    return this;
  }

}

export default TryStatement;