import * as Internal from '../internal.js';

/**
 *
 *
 * 
 * @class WhileStatement
 * @extends {Internal.Statement}
 * 
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#whilestatement}
 */
class WhileStatement extends Internal.ConcreteStatement {

  constructor(parentNode, test) {
    super(parentNode)
    this.test = this.apply(test, new Internal.PlaceholderExpression(this))
    this.body = null
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      test: this.test.node,
      body: this.body.node
    }
  }
  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   * @memberof WhileStatement
   */
  Do(cb) {
    this.body = this.apply(cb, new Internal.PlaceholderStatement(this))
    return this
  }
  /**
   *
   *
   * @param {Internal.StatementGeneratorCb} cb
   * @returns
   * @memberof WhileStatement
   */
  DoBlock(cb) {
    this.body = this.apply(cb, new Internal.BlockStatement(this))
    return this
  }
}

export default WhileStatement