import * as Internal from '../internal.js';

/**
 * 
 * @class BreakStatement
 * @extends {Internal.Statement}
 * @see{@link https://github.com/estree/estree/blob/master/es5.md#breakstatement}
 */
class BreakStatement extends Internal.ConcreteStatement {
  constructor(node, label) {
    super(node);
    this.label = label ? new Internal.Identifier(this, label) : null;
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      label: this.label ? this.label.node : null
    }
  }

}

export default BreakStatement;