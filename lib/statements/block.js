import * as Internal from '../internal.js';

/**
 *
 * @class BlockStatement
 * @extends {Internal.StatementContainer}
 * 
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#blockstatement}
 */
class BlockStatement extends Internal.StatementContainer {

  constructor(node, callback) {
    super(node);
    this.body = [];
    if (callback) this.apply(callback, this);
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      body: this.body.map(s => s.node)
    }
  }

  set statement(s) {
    this.body.push(s);
    return s;
  }
}

export default BlockStatement;