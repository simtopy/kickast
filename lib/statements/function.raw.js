import * as Internal from '../internal.js';

/**
 *
 * @class RawFunctionDeclaration
 * @extends {Internal.Statement}
 */
class RawFunctionDeclaration extends Internal.Declaration {
  constructor(node, f) {
    super(node)
    let comments = []
    let ast = Internal.parse(f.toString(), {
      onComment: function(block, text, start, end) {
        comments.push({
          type: block ? "Block" : "Line",
          value: text,
          start,
          end
        })
      }
    })
    this.function = ast.body[0]
    this.function.body.comments = comments
    if (this.function.type != "FunctionDeclaration") throw Error("argument should be a function declaration")
    let name = this.function.id.name
  }

  get _node() {
    return this.function
  }
}
export default RawFunctionDeclaration;