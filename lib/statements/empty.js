import * as Internal from '../internal.js';

/**
 *
 * @class EmptyStatement
 * @extends {Internal.Statement}
 */
class EmptyStatement extends Internal.ConcreteStatement {
  constructor(node) {
    super(node);
  }
}

export default EmptyStatement;