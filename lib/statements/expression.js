import * as Internal from '../internal.js';

/**
 * @class ExpressionStatement
 * @extends {Internal.Statement}
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#expressionstatement}
 * 
 * @property {Internal.PlaceholderExpression} [_expression]
 */
class ExpressionStatement extends Internal.ConcreteStatement {

  constructor(parentNode) {
    super(parentNode);
    this._expression = new Internal.PlaceholderExpression(this);
  }

  get _node() {
    const supernode = super._node
    return {
      ...supernode,
      expression: this._expression.node
    }
  }

  set expression(e) {
    this._expression = e;
    return e;
  }

  get expression() {
    return this._expression;
  }

}

export default ExpressionStatement;