import * as Internal from '../internal.js';

/**
 *
 * A placeholder Statement, that is a pure virtual entity, for syntaxic convenience purpose
 * when branching with callbacks
 * 
 * @class PlaceholderExpression
 *
 * @see {@link https://github.com/estree/estree/blob/master/es5.md#statements}
 * 
 * @extends {Internal.StatementContainer}
 **/
class PlaceholderStatement extends Internal.StatementContainer {

  /**
   * builds a placeholder statement.
   **/
  constructor(parentNode) {
    super(parentNode)
  }

  get _node() {
    if (!this.statement) throw Error(`A placeholder of type ${this.constructor.name} is remaining, child of node of type ${this.concreteParent.constructor.name}. Therefore, I cannot generate a valid AST.`)
    const content = this.statement.node
    //const supernode = super._node
    return {
      ...content
    }
  }

  /**
   * set the concrete underlying statement
   **/
  set statement(s) {
    //s.parent = this.concreteParent
    this._statement = s
  }

  /**
   * get the concrete underlying statement
   **/
  get statement() {
    return this._statement
  }
}

export default PlaceholderStatement;