import assert from 'assert';

//Functions useful for debugging and custom aliases binding

export default Object.freeze({
  _getValidFunctionList: function(sourceObject) {
    if ([null, undefined].includes(sourceObject)) return []
    else return Object.getOwnPropertyNames(sourceObject)
      .filter((k) => k != "constructor")
      .filter((k) => sourceObject[k] instanceof Function)
  },

/*   _getClassProto: function(c) {
    return Object.getPrototypeOf(new c())
  }, */

  /**
   * Bind method to a class
   * 
   * @param targetClass - the class to bind the function to
   * @param identifier - the func identifier inside the bound class
   * @param sourceFunction - the func to bind.
   *
   **/
  bindFunction: function(targetClass, identifier, sourceFunction) {
    assert(sourceFunction instanceof Function, `${sourceFunction} is not a valid function`)
    targetClass.prototype[identifier] = sourceFunction
  },

  /**
   * Bind Object API to a class
   **/
  bindObject: function(targetClass, sourceObject) {
    this._getValidFunctionList(sourceObject)
      .forEach((k) => {
        this.bindFunction(targetClass, k, sourceObject[k])
      })
  },

});