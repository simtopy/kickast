import { configurator as kickastConfigurator } from './index.js'
import { strict as Assert } from 'assert'
import { jest } from '@jest/globals'

const AsyncFunction = Object.getPrototypeOf(async function () { }).constructor

const testJsDoc = (expected, res) => {
  expected.forEach(e => Assert(res.includes(e), `${e} IN \n${res}`))
}

const testMissingJsDoc = (expected, res) => {
  expected.forEach(e => Assert(!res.includes(e), `${e} IN \n${res}`))
}

const testJsDocRegex = (regexp, res, testCb) => {
  const match = res.match(regexp)
  Assert(testCb(match), `FAILED TEST \n"""\n${testCb.toString()}\n"""\n with REGEXP ${regexp} on \n${res}.\n MATCH RESULT ${match ? match.toString() : null}`)
}

const Kickast = kickastConfigurator({
  autoJsDoc: true,
  logOutput: true,
  jsDocHints: true,
  debug: true,

  // custom aliases
  ModuleAliases: {
    CustomModuleAlias() { }
  },
  StatementAliases: {
    Actions(...args) {
      const obj = args.pop()
      return this.This("props", "Actions").Call(...args, e => e.Object(obj))
    },
    Dispatch(obj) {
      return this.This('props', 'dispatch')
        .Call(e => e.Object(obj))
    }
  },
  ExpressionAliases: {
    CustomExpressionAlias() { }
  }
})

function toSource(tree) {
  return tree.toString({ comments: true })
}

async function exec(tree, tests) {
  let src = toSource(tree)
  try {
    let result = await (new AsyncFunction(src))()
    tests(result)
  } catch (err) {
    console.log(src)
    throw err
  }
}

describe("kickast.Program generation", function () {

  beforeEach(function () {
    this.prgm = Kickast()
  })

  it("write a block comment", function () {
    this.prgm.comment([
      "~~~ es7 test suite ~~~",
      "This source code was d with the es7 grammar",
      "Author: Ewen MARECHAL"
    ])
  })

  it("write a directive", function () {
    this.prgm.Directive("a directive")
      .comment("A directive statement")
  })

  it("returns a value", function () {
    this.prgm.Return(true)
    // result should be true
    return exec(this.prgm, r => Assert(r))
  })

  it("declares variables", function () {
    this.prgm.Declare("var")
      .comment("Simple variable declaration")
      .Identifier("a", 0)
      .Identifier("b", {})

    this.prgm.Return().Array().Identifiers("a", "b")
    // a should be 0, b should be an object
    return exec(this.prgm, ([a, b]) => {
      Assert.equal(a, 0)
      Assert(typeof b === "object")
    })
  })

  it("assigns a variable using Not operator", function () {
    this.prgm.Let("r", false)
    this.prgm.Id("r").Assign().Not().False()
      .comment("Expression using Not and False aliases")
    this.prgm.Return().Id("r")
    // result should be true
    return exec(this.prgm, r => Assert.equal(r, true))
  })

  it("assigns a variable using Ternary operator", function () {
    this.prgm.Return().Ternary(e => e.True(), "right", "wrong")
      .comment("Expression using Not, False aliases...")
    // result should be right
    return exec(this.prgm, r => Assert.equal(r, "right"))
  })

  it("tests ObjectExpression", function () {
    this.prgm.Let("shorthand", null)
    this.prgm.Return().Object(o => {
      o.comment([
        "A block comment...",
        "... in an object !"
      ])
      o.Spread(e => e.Object())
      o.Static("number", 42, "a literal").comment("a literal")
      o.Shorthand("shorthand", "A shorthand").comment("A shorthand")
      o.Static("static", true, "a static property").comment("a static property")
      o.Computed(e => e.Literal("computed"), true).comment("a computed property")
    })

    // result should be
    return exec(this.prgm, r => Assert.deepEqual(r, {
      number: 42,
      shorthand: null,
      static: true,
      computed: true
    }))
  })

  it("tests ObjectExpression", function () {
    this.prgm.Let("a", e => e.Object().Method("do", false, [], b => b.Return(42)))
    this.prgm.Return().Id("a", "do").Call()

    // result should be
    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests ArrayExpression", function () {
    this.prgm.Let("zero", null)
    this.prgm.Return().Array()
      .Identifiers("zero")
      .Literals(1, "two")
      .Expressions(e => e.Literal("three"))

    // result should be [null, 1, "two", "three"]
    return exec(this.prgm, r => Assert.deepEqual(r, [null, 1, "two", "three"]))
  })

  it("tests ArrayPattern", function () {
    this.prgm.LetArray(pattern => pattern
      .Identifier("a")
      .Identifier("b", e => e.Null())
      .Rest("c"), [1, 2, 3, 4, 5])

    this.prgm.Return().Array().Expressions(
      e => e.Id("a"),
      e => e.Id("b"),
      e => e.Id("c")
    )

    // result should be [1, 2, [3, 4, 5]]
    return exec(this.prgm, r => Assert.deepEqual(r, [1, 2, [3, 4, 5]]))
  })

  it(" array from another array", function () {
    let source = [0, 1, 2, 3]
    this.prgm.Return()
      .comment(" array from another array")
      .Array().From(source, (e, data, index) => e.Value(data + index))
    // result should be [0 + 0, 1 + 1, 2 + 2, 3 + 3]
    return exec(this.prgm, r => Assert.deepEqual(r, [0, 2, 4, 6]))
  })

  it("sets object using the callback", function () {
    this.prgm.Return()
      .comment("Object can be set with a callback")
      .Object(o => {
        for (let i = 1; i < 4; i++) {
          o.Static(`prop${i}`, i * i, `the square of ${i}`)
        }
      })
    return exec(this.prgm, r => Assert.deepEqual(r, {
      prop1: 1,
      prop2: 4,
      prop3: 9,
    }))
  })

  it("tests MemberExpression", function () {
    this.prgm.Let("a", {})
    this.prgm.Id("a").Member("b").Assign().Value({})
    //.comment("Object can be access")
    this.prgm.Set("a", "b", e => e.Literal("c")).Value(42)
    this.prgm.Return().Id("a", "b", "c")

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests Optional Member Expression", function () {
    this.prgm.Let("a", {})
    this.prgm.Id("a").Member("b").Assign().Value({})
    //.comment("Object can be access")
    this.prgm.Set("a", "b", e => e.Literal("c")).Value(42)
    this.prgm.Let("e", e => e.Id("a").OptionalMember("b"))
    this.prgm.Return().Id("a", "b", "c")
    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests Index aliase", function () {
    this.prgm.Let("a", [])
    this.prgm.Let("zero", 0)
    this.prgm.Id("a").Index(0).Assign().Array()
    this.prgm.Id("a").Index(0, 0).Assign().Value(42)
    this.prgm.Return().Id("a").Index(e => e.Id("zero"), "zero")

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests IfStatement ", function () {
    this.prgm.If(e => e.True())
      .Then(s => s.Return().True())
    console.log(this.prgm.toString())
    return exec(this.prgm, r => Assert.equal(r, true))
  })

  it("tests ElseStatement ", function () {
    this.prgm.If(e => e.False())
      .Then(s => s.Return().True())
      .Else(s => s.Return().False())

    return exec(this.prgm, r => Assert.equal(r, false))
  })

  it("tests FunctionDeclaration", function () {
    this.prgm.Function("f", [], b => {
      b.Return(42)
    }).comment("function declaration")
    this.prgm.Return().Id("f").Call()

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests RawFunctionDeclaration", function () {
    this.prgm.RawFunction(function f() {
      /*
        RawFunctionDeclaration
      */
      return 42
    }).comment("raw function declaration")
    this.prgm.Return().Id("f").Call()

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests FunctionExpression", function () {
    this.prgm.Let("f", e => e.Function([], b => b.Return(42)))
    this.prgm.Return().Id("f").Call()

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests RawFunctionExpression", function () {
    this.prgm.Let("f", e => e.RawFunction(function () {
      /*
        RawFunctionExpression
      */
      return 42
    }))
    this.prgm.Return().Id("f").Call()

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests RawFunction with Function", function () {
    this.prgm.Let("o", e => {
      e.Object().Static("f", e => e.RawFunction(new Function('', 'return 42')))
    })
    this.prgm.Return().Id("o", "f").Call()

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests ArrowExpression", function () {
    this.prgm.Let("f", e => e.Arrow([], b => b.Return(42)))
    this.prgm.Return().Id("f").Call()

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests RawArrowExpression", function () {
    this.prgm.Let("f", e => {
      e.RawArrow(() => {
        /*
          RawArrowExpression
        */
        return 42
      })
    })
    this.prgm.Return().Id("f").Call()

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("tests async code", function () {
    this.prgm.Function("sleep", "delay", b => {
      b.Return().Promise(b => {
        b.Identifier("setTimeout")
          .Call(e => e.Id("resolve"), e => e.Id("delay"))
      })
    }).comment("sleep function resolve after a delay (ms)")
    this.prgm.AsyncFunction("myAsync", [], b => {
      b.Await().Id("sleep").Call(10)
      b.Return().Value("done")
    }).comment("await sleep(10) within async function")

    this.prgm.Return().Id("myAsync").Call()

    return exec(this.prgm, r => Assert.equal(r, "done"))
  })

  it("(inline) arrow expression", function () {
    this.prgm.Let("arrow", e => e.Arrow("x").Id("x").x().Value(2))
      .comment("(inline) arrow expression")
    this.prgm.Return().Id("arrow").Call(21)

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("(async) arrow expression", function () {

    this.prgm.Let("arrow", e => e.AsyncArrow().Await().Resolved(42))
      .comment("(async) arrow expression")
    this.prgm.Return().Id("arrow").Call()

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("s a switch statement", function () {
    this.prgm.Let("test", "right")
    this.prgm.Switch(e => e.Id("test"))
      .comment("a switch statement")
      .Case(e => e.Literal("right"), b => {
        b.comment("single statement case")
        b.Break()
      })
      .Case(e => e.Literal("left"), b => {
        b.comment("A multiple statement case")
        b.Break()
      })
      .Default(b => {
        b.comment("the default case")
        b.Identifier('console').Member('log').Call("Error")
        b.Break()
      })

    return exec(this.prgm, r => {
      //Assert.equal(await r, "done")
    })
  })

  it("Try Catch Finally", function () {
    this.prgm.Try(b => {
      b.Let("e", e => e.Literal("try this"))
    })
      .Catch(e => e.Identifier('err'), b => {
        b.Identifier('console').Member('log').Call(e => e.Literal("Error"))
      })
      .Finally(b => b.Return().Value("done"))

    return exec(this.prgm, async r => Assert.equal(r, "done"))
  })

  it("use binary operator", function () {
    this.prgm.Declare()
      .Identifier("op", e => e.Value(42).And().True().And().True())
  })

  it("use spread operator", function () {
    this.prgm.Set("a").Array()
      .Expressions(e => e.Spread().Array())
  })

  it("test computed members", function () {
    this.prgm.Let("foo")
    this.prgm.Id("foo", "bar").Index("lol").Assign().Value(42)
  })

  it("loop for... in...", function () {
    this.prgm.Let("array", [])
    this.prgm.Let("range", e => e.Array().Literals(1, 2, 3, 4))
    this.prgm.ForLetIn("e", e => e.Id("range"), b => {
      b.Id("array", "push").Call(e => e.Id("e"))
    })
    this.prgm.Return().Id("array")

    return exec(this.prgm, r => Assert.deepEqual(r, ["0", "1", "2", "3"]))
  })

  it("loop for... of...", function () {
    this.prgm.Let("sum", 0)
    this.prgm.Let("range", e => e.Array().Literals(9, 10, 11, 12))
    this.prgm.ForLetOf("e", e => e.Id("range"), b => b.Id("sum").Increment().Id("e"))
    this.prgm.Return().Id("sum")

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("while loop", function () {
    this.prgm.Let("sum", 0)
    this.prgm.While(e => e.Id("sum").lt().Value(42))
      .DoBlock(b => {
        b.Id("sum").Increment().Value(1)
      })
    this.prgm.Return().Id("sum")

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("do … while loop", function () {
    this.prgm.Let("sum", 0)
    this.prgm.Do(b => b.Id("sum").Increment().Value(1))
      .While(e => {
        const be = e.Id("sum").lt().Value(42)
      })
    this.prgm.Return().Id("sum")

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("for loop", function () {
    this.prgm.Let("sum", 0)
    this.prgm.For('i', 9, e => e.Id('i').le().Value(12), e => e.Id('i').Increment().Value(1))
      .DoBlock(b => b.Id("sum").Increment().Id('i'))
    this.prgm.Return().Id("sum")

    return exec(this.prgm, r => Assert.equal(r, 42))
  })

  it("test promise aliases", function () {
    this.prgm.Expression().Resolved()
      .Then(e => e.Arrow([], s => s.Return().Void()))
    return exec(this.prgm, r => { })
  })

  it("test destructuration", function () {
    this.prgm.LetPattern(p => {
      p.Property("foo")
      p.Property("bar", 21)
    }, {
      foo: 21
    })
    this.prgm.Return().Id("foo").p().Id("bar")
    return exec(this.prgm, r => {
      Assert.equal(r, 42)
    })
  })

  it("test template literals", function () {
    this.prgm.Const("token", "bar")
    this.prgm.Const("first", "first")
    this.prgm.Const("second", "second")
    this.prgm.Const("third", "third")
    this.prgm.Let("string1", e => e.TemplateLiteral().Part("foo").Id("token").Part("baz")) //expression in
    this.prgm.Let("string2", e => e.TemplateLiteral().Id("token").Id("token").Id("token")) //ordered expression
    this.prgm.Let("string3", e => e.TemplateLiteral()) // empty
    this.prgm.Let("string4", e => e.TemplateLiteral().Part("first").Id("second").Id("third")) // mixin
    this.prgm.Let("string5", e => e.TemplateLiteral().Id("first").Part("second").Id("third"))
    this.prgm.Let("string6", e => e.TemplateLiteral().Id("first").Id("second").Part("third")) // unordered
    this.prgm.Let("string7", e => e.TemplateLiteral((t) => { // callback style
      t.Part("first")
      t.Id("second")
      t.Part("third")
    }))
    this.prgm.Return().Id("string1")
      .plus().Identifier("string2")
      .plus().Identifier("string3")
      .plus().Identifier("string4")
      .plus().Identifier("string5")
      .plus().Identifier("string6")
      .plus().Identifier("string7")
    return exec(this.prgm, r => {
      Assert.equal(r, "foobarbaz" + "barbarbar" + "firstsecondthird" + "firstsecondthird" + "firstsecondthird" + "firstsecondthird")
    })
  })

  it("test tagged template literals", function () {

    this.prgm.Const("token", "token")

    this.prgm.RawFunction(function tag(strings, ...exprs) {
      return strings.join("_")
    }).comment("tag function")

    this.prgm.Const("interpolated", e => e.TaggedTemplateExpression(e => e.Id("tag"), e => {
      e.TemplateLiteral().Part("foo").Id("token").Part("bar")
    }))

    //.TaggedTemplate().Part("first"))

    /*
    this.prgm.Const("token", "bar")
    this.prgm.Const("first", "first")
    this.prgm.Const("second", "second")
    this.prgm.Const("third", "third")
    this.prgm.Let("string1", e => e.TemplateLiteral().Part("foo").Id("token").Part("baz")) //expression in
    this.prgm.Let("string2", e => e.TemplateLiteral().Id("token").Id("token").Id("token")) //ordered expression
    this.prgm.Let("string3", e => e.TemplateLiteral()) // empty
    this.prgm.Let("string4", e => e.TemplateLiteral().Part("first").Id("second").Id("third")) // mixin
    this.prgm.Let("string5", e => e.TemplateLiteral().Id("first").Part("second").Id("third"))
    this.prgm.Let("string6", e => e.TemplateLiteral().Id("first").Id("second").Part("third")) // unordered
    this.prgm.Let("string7", e => e.TemplateLiteral((t) => { // callback style
      t.Part("first")
      t.Id("second")
      t.Part("third")
    }))
    this.prgm.Return().Id("string1")
      .plus().Identifier("string2")
      .plus().Identifier("string3")
      .plus().Identifier("string4")
      .plus().Identifier("string5")
      .plus().Identifier("string6")
      .plus().Identifier("string7")
      */

    this.prgm.Return().Id("interpolated")

    return exec(this.prgm, r => {
      Assert.equal(r, "foo_bar")
    })
  })
})

describe("Classes", function () {

  beforeEach(function () {
    this.prgm = Kickast()
  })

  it("class declaration", function () {

    this.prgm.Class("BaseClass", c => {

      c.comment("Base class declaration")

      c.Constructor(["value"], b => {
        b.This("_value").Assign().Id("value")
      }, "class constructor")

      //c.StaticProperty("fixed", "n", 0, "static property")
      /*
      c.StaticMethod("setFixed", "n", b => {

      }, "static method")
      */

      c.Getter("value", b => {
        b.Return().This("_value")
      }, "value getter")

      c.Setter("value", "value", b => {
        b.This("_value").Assign().Id("value")
      }, "value setter")
    })

    this.prgm.Class("DerivedClass", c => {
      c.Extends("BaseClass")
      c.Constructor(["value"], b => {
        b.comment("This is the constructor body")
        b.Identifier("super").Call(e => e.Id("value"))
      })
      c.Method("squared", [], b => {
        b.Return().This("value").times().This("value")
      }).comment("Derived class declaration")
    })

    this.prgm.Return().Array().Identifiers("BaseClass", "DerivedClass")

    return exec(this.prgm, ([BaseClass, DerivedClass]) => {
      let r = new DerivedClass(2)
      Assert.equal(r.squared(), 4)
      Assert(r instanceof BaseClass)
      Assert(r instanceof DerivedClass)
    })
  })
})

/* describe("Modules", function() {

beforeEach(function() {
  this.prgm = Kickast()
})

it("export default function", async function() {

  this.prgm.ExportDefault(e => e.Function([], b => {}))

  let module = await this.prgm.toModule()
  Assert.equal(typeof module.default, "function")
})

it("export default class", async function() {

  this.prgm.ExportDefault(e => e.Class("MyClass", c => {}))

  let module = await this.prgm.toModule()
  Assert.deepEqual(typeof module.default, "function")
})

it("export symbol", async function() {

  this.prgm.Class("MyClass")
  this.prgm.ExportDefault(e => e.Id("MyClass"))

  let module = await this.prgm.toModule()

  Assert.deepEqual(typeof module.default, "function")
})

it("export named declaration", async function() {

  this.prgm.ExportNamedDeclaration(s => s.Function("validate", [], b => {
    b.Return(true)
  }))

  let { validate } = await this.prgm.toModule()

  Assert.equal(typeof validate, "function")
  Assert.equal(validate(), true)
})

}) */

describe("Extension", function () {

  beforeEach(function () {
    this.prgm = Kickast()
  })

  it("binds custom aliases", function () {
    this.prgm.Dispatch(o => {
      o.Static("type", "SET_ELEMENT")
      o.Static("elem", e => e.True())
    })
    this.prgm.Actions("form", "resetValid", o => {
      o.Static("formKey", "myForm")
    })
  })

})
/*
describe("React Support", function() {

  beforeEach(function() {
    this.prgm = kickast.Program()
  })

  it("import react modules", function() {
    this.prgm.Import().Default("React").Local(["useEffect", "Effect"], "useState").From("react")
    this.prgm.Import().Local("useReducer").From("react")
    this.prgm.Import().Local("connect").From("react-redux")
  })

})*/

describe("JSDoc", function () {

  it("Simple Tags and types", function () {
    this.prgm = Kickast().JSDoc(jsd => {
      jsd.Author("Lucas Vincent", "lucas.vincent@skales.fr", "the guy who made this")
      jsd.Type("string", "mystring", "my own custom string")
      jsd.ImportType("myType", "../toto.js", "myType")
    })
    this.prgm.Let("myString", e => e.Literal("string"))
  })

  it("jsDoc function declaration", function () {
    this.prgm = Kickast()
    this.prgm.Let("myFunction", e => e.Function(["a", "b", "c"], b => b))
      .JSDoc(c => {
        c.SeeLink('https://example.com')
        c.Type(t => t.TypedFunction(p => {
          p.Param(t => t.Union(u => {
            u.String
            u.Number
          }))
          p.Param(t => t.String)
          p.Param(t => t.Maybe.String)
        }, r => r.Void), "myFunction")
      })
  })

  it("jsdoc kickast.Program", function () {

    this.prgm = Kickast()
    this.prgm.JSDoc(c => {
      c.ImportType("customType", "myModule", "myClass", "I import my awesome class")
      c.Description("My awesome function")
      c.SeeLink('https://example.com')
      c.Param(p => p.Just.String, "a")
      c.Param(p => p.Array.UserDefinedType("customType"), "b")
      c.Param(p => p.Import("myModule", "default"))
      c.Returns(p => p.Void)
    })
    this.prgm.Function("myFunction", ["a", "b", "c"], b => b)

  })
})

describe("Typing features", function () {
  it("A Documented Object", function () {

    this.prgm = Kickast()
    this.prgm.Let("myObject", e => e.Object(o => {
      o.Static("myProperty", e => e.Literal("content"), null).Type(t => t.Just.String).Describe("An amazing property")
    }))
    testJsDoc([
      '/**',
      '* @type {object}',
      '**/',
    ], this.prgm.toString())
  })

  it("Variable declaration typing", function () {
    this.prgm = Kickast()
    this.prgm.Let("avar").Type(t => t.String).Describe("my variable")
    testJsDoc([
      '* @type {string}',
      '* my variable'
    ], this.prgm.toString())
  })

  it("A Documenter Object", function () {
    this.prgm = Kickast()
    this.prgm.Let("myObject", e => e.Object(o => {
      //o.Type("CustomObject")
      o.Describe("My object is the best object")
      o.Static("myProperty", e => e.Literal("content"), null).Type(t => t.Just.String)
      o.Static("subObject", e => e.Object(o2 => {
        o2.Static("test", null).Type(t => t.NotNullable.String)
      }))
      o.Method("myArrow", true, p => {
        p.Id("p").Type(p => p.String)
      }, b => {
        b.Identifier("console").Member("log").Call("p")
        b.Return(e => e.Identifier("p").Type(t => t.String))
      })
        .Describe("An amazing function")
    }))
    testJsDoc([
      '/**',
      '* @type {object}',
      '* My object is the best object',
      '**/',
      '* @method',
      '* An amazing function',
      '* @param {string} p',
      '* @returns {string}'
    ], this.prgm.toString())
    testMissingJsDoc([
      ' @type {*}'
    ], this.prgm.toString())
  })

  it("A Documented function", function () {
    this.prgm = Kickast()
    this.prgm.Function("myFunction", p => {
      p.Id("param").Type(t => t.String).Describe("this is a function param")
      p.Id("p2").Type(t => t.Number).Describe("Another param")
    }, b => {
      b.Id("console").Member("log").Call("param")
      b.Return(e => e.Identifier("p2")).Type(t => t.Number)
      b.Return(e => e.Id('param').Type(t => t.String))
    })
    this.prgm.Function("MyOtherFunction", p => {
      p.Id("toto")
      p.Id("tata")
    }, b => {

    })
    this.prgm.toString()
    testJsDoc([
      '/**',
      '* @param {string} param - this is a function param',
      '* @param {number} p2 - Another param',
      '* @returns {(number|string)}',
      '**/',
    ], this.prgm.toString())
  })

  it("A documented class", function () {
    this.prgm = Kickast()
    this.prgm.Class("myClass", c => {
      c.Describe("This is my own custom class")
      c.Extends("Object")
      c.Constructor(p => {
        p.Id("param").Type(t => t.String)
      }, b => {
        b.Identifier("console").Member("log").Call("param")
      })

      c.Getter("truc", s => {
        s.Return(e => e.Id("truc")).Type(t => t.String)
      }).Public()

      c.Setter("machin", p => p.Id("param").Type(t => t.String), b => {
        b.Empty().comment("test")
      })
    })

    testJsDoc([
      '/**',
      '* @class',
      '**/',

      '/**',
      '* @constructor',
      '* @param {string} param',
      '**/',

      '/**',
      '* @public',
      '* @getter',
      '* @returns {string}',
      '**/',

      '// test',
    ], this.prgm.toString())
  })

  it("test multiple returns statement in function", function () {
    this.prgm = Kickast()
    this.prgm.Function("func", [], b => {
      b.If(e => e.True()).Then(s => s.Return(e => e.Id("toto")).Type(t => t.Number))
        .Else(e => e.Return(e => e.Id("tata").Type(t => t.Null)))
      b.Return(e => e.Literal("[]")).Type(t => t.Array.String)
    })
    testJsDoc([
      '* @returns {(number|null|string[])}'
    ], this.prgm.toString())
  })
})

describe("Comments", function () {
  it("Comment in a block", function () {
    this.prgm = Kickast()
    this.prgm.comment("Top level comment")
    this.prgm.If(e => e.True()).ThenBlock(s => {
      s.comment("comment in a block")
      s.Let("toto", 3)
      s.Let("aaaa", e => e.FunctionExpression(false, [], s => {
        s.comment("Comment in a function")
      })).comment("Comment on a function")
      this.prgm.Let("myObject", e => e.Object(o => {
        o.comment("Comment on a object")
        o.Static("prop", 3, null).comment("An awesome property")
      }))
      this.prgm.Class("myClass", c => {
        c.comment("comment on class")
        c.Constructor([], b => b)
        c.Method("myMethod", [], b => b).comment("Comment in class method")
      })
    })
  })
})

describe("Usability", function () {
  it("callback without return value", function () {
    this.prgm = Kickast()
    const cb = e => {
      e.Id("tata")
    }
    this.prgm.Let("toto", e => e.Object(o => {
      o.Static("myProp", e => cb(e))
    }))

    this.prgm.toString()
  })
  it("no jsdoc in inappropriate places", function () {
    this.prgm = Kickast()
    this.prgm.Let("projectUser", e => e.Id("projectUsers", "find").Call(e => {
      let toto = e.Arrow("pu")
      toto.Id("pu", "user").Equals().Id("userUid")
    }))
    this.prgm.Let('k', e => e.Await().Get("ns", "select").Call('k', e => {
      e.Object(o => o.Static('k', e => e.Id("userUid")))
    }))
    this.prgm.For('i', 9, e => e.Id('i').le().Value(12), e => e.Id('i').Increment().Value(1))
      .DoBlock(b => b.Id("sum").Increment().Id('i'))

    testMissingJsDoc([
      '* @object',
      '* @function'
    ], this.prgm.toString())
  })

  it("jsdoc duplicates", function () {
    this.prgm = Kickast()
    this.prgm.Let("myVar", e => e.Object())
    testJsDocRegex(/@type/gm, this.prgm.toString(), r => {
      /* beautify preserve:start */
      const res = r?.length === 1
      /* beautify preserve:end*/
      return res
    })
  })
  it("jsdoc for", function () {
    this.prgm = Kickast()
    this.prgm.For("i", 0, e => e.Id("i").lt().Literal("10"), e => e.Id("i").Increment().Value(1)).DoBlock(b => b.Let("i", "3"))
    testJsDocRegex(/@type/gm, this.prgm.toString(), r => {
      const res = r === null
      return res
    })
  })

  it("jsdoc override", function () {
    this.prgm = Kickast()
    this.prgm.Const("toto", e => e.Function(p => p, b => b.Empty())).JSDoc(jsd => {
      jsd.Author("me")
      jsd.Type(t => t.String)
    }, true).flag("FLAG")
    testMissingJsDoc([
      '@type {*}'
    ], this.prgm.toString())
  })
})